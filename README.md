# degreeforme

CS373 Project

Name: Ali Kedwaii
EID: mak3799
GitLabID: xirothehero

Name: Kaitlyn Bush
EID: rtb799
GitLabID: kbush1563

Name: Juan Carlos Moreno
EID: jm84372
GitLabID: jcmg

Name: Nidhi Rathod
EID: ndr478
GitLabID: nidhirathod

Name: Dheeraj Nuthakki
EID: dkn354
GitLabID: grexia

Name: Courtney Huynh 
EID: clh4759 
GitLabID: qwertyhwin

Git SHA: a370f168df3dd306c11ebb951195b7c8f7e199e9

GitLab Pipelines: https://gitlab.com/xirothehero/degreeforme/pipelines

Website: https://www.degreeforme.me

s3 bucket URL: http://www.degreeforme.me.s3-website-us-east-1.amazonaws.com/

Estimated Completion Time (hours: int):
Ali - 15
Kaitlyn - 15
Juan Carlos - 15
Nidhi - 15
Dheeraj - 15
Courtney - 15

Actual Completion Time (hours: int):
Ali - 18
Kaitlyn - 17
Juan Carlos - 17
Nidhi - 18
Dheeraj - 17
Courtney - 22
Comments: Phase 3 was interesting. Searching was the easier of requirements to implement; however, filtering and sorting were tricky.