FROM python:3

RUN apt-get update

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -

RUN apt-get install -y npm
RUN apt-get install -y nodejs

RUN npm install @material-ui/core
RUN npm install @material-ui/icons
RUN npm install @material/top-app-bar
RUN npm install axios
RUN npm install bootstrap
RUN npm install react
RUN npm install react-dom
RUN npm install react-router
RUN npm install react-router-dom
RUN npm install react-scripts
RUN npm install typeface-roboto
RUN npm install typescript

RUN pip install --upgrade pip
RUN pip --version

RUN pip install flask
RUN pip install flask_sqlalchemy
RUN pip install flask_restless
RUN pip install sqlalchemy

RUN git clone https://gitlab.com/xirothehero/degreeforme

CMD bash
