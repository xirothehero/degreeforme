import json
import pickle
import re
import requests
from sys import stdout
from typing import IO
import time
import urllib.request
from bs4 import BeautifulSoup as BS
import selenium
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait

# driver = webdriver.Chrome(executable_path='/mnt/c/workspace/chromedriver.exe')

def read_degree_data():
    degrees = pickle.load(open("../api_code/API Data/refined_degree_data2.p", "rb"))
    degree_list = [d['degree'] for d in degrees]
    return degree_list

def get_shadow_root(url):
    driver.get(url)
    driver.implicitly_wait(10) # seconds
    try:
        element = driver.find_elements_by_tag_name('data-career')[2]
        shadow_root = driver.execute_script('return arguments[0].shadowRoot', element)
        shadow_html = driver.execute_script('return arguments[0].innerHTML',shadow_root)
    except Exception:
        shadow_html = None
    return shadow_html

def get_jobs(degrees):
    degree_info_list = []
    for degree in degrees:
        deg = degree.split('(')[0].strip().lower().replace(' ','-')
        job_dict = {}
        url = "https://www.talentdesk.com/career-finder/"+ deg +"-degree-jobs"
        print(url)
        shadow = get_shadow_root(url)
        print(shadow)
        if shadow is None:
            job_dict['N/A'] = 0
        else:
            soup = BS(shadow, "html.parser")
            job_table = soup.find('table').find('tbody')
            job_rows = job_table.findAll('tr')
            for j in job_rows[0:3]:
                job = j.findAll('td')[1].find('div').find('a')
                job = re.sub('<[^<]+?>', '', str(job))
                salary = re.sub('<[^<]+?>', '', str(j.findAll('td')[2]))
                salary = int(salary.split('$')[1].split(' ')[0].replace(',',''))
                job_dict[job] = salary
        degree_info = {'degree':degree, 'jobs':job_dict}
        degree_info_list.append(degree_info)
    
    print(degree_info_list)

def add_jobs():
    deg_data_2 = open("../api_code/API Data/refined_degree_data2.p", "rb")
    degree_dict = pickle.load(deg_data_2)
    deg_data_2.close()

    job_dict = { 
        'Petroleum engineering' : ['Petroleum engineer', 'Field engineer'], 
        'Systems engineering' : ['Electrical systems engineer', 'Industrial engineer'], 
        'Computer science (CS) and engineering' : ['Software developer', 'Computer and information systems manager'], 
        'Building construction management' : ['Construction manager', 'Construction project manager'], 
        'Mathematics' : ['Actuary', 'Computer systems analyst'], 
        'Physical therapy' : ['Physical therapist', 'Physician'], 
        'Business and marketing' : ['Accountant', 'marketing and sales manager'], 
        'Technical communication' : ['Technical writer', 'Technical support agent'], 
        'Philosophy' : ['Professor', 'Social worker'], 
        'Architecture' : ['Architect', 'Interior designer'], 
        'Biological sciences' : ['Biological scientist', 'Physician'], 
        'Transportation and logistics management' : ['Operations manager', 'Mechanics and service technician'], 
        'Foreign languages' : ['Translator', 'Teacher'], 
        'History' : ['Anthropologist', 'Teacher'], 
        'Communication' : ['Editor', 'Public relations specialist'], 
        'Agriculture' : ['Agricultural engineer', 'Food scientist'], 
        'Public administration' : ['Education administrator', 'Human resources specialist'], 
        'Medical laboratory technology' : ['Clinical laboratory technician', 'Health services manager'], 
        'Visual arts (VA)' : ['Commercial artist', 'Animator'], 
        'Psychology' : ['Psychologist', 'Counselor'], 
        'Health' : ['Physician', 'Nurse'], 
        'Social science' : ['Social worker', 'Counselor'], 
        'Education' : ['Teacher', 'Education administrator'], 
        'Legal studies' : ['Lawyer', 'Legal support worker'], 
        'Humanities' : ['Professor', 'Writer'], 
        'Family and consumer science' : ['Social worker', 'Dietitian'], 
        'Culinary arts' : ['Executive chef', 'Culinary assistant'], 
        'Parks and recreation management' : ['Facilities manager', 'Recreation supervisor'], 
        'Theology' : ['Clergy', 'Religious activities & education director']
    }

    for d in degree_dict:
        d['jobs'] = job_dict[d['degree']]

    # print(degree_dict)

    deg_data_3 = open("../api_code/API Data/refined_degree_data3.p", "wb")
    pickle.dump(degree_dict, deg_data_3)
    deg_data_3.close()

def check_pickle():
    deg_data = open("../api_code/API Data/refined_degree_data3.p", "rb")
    deg_dict = pickle.load(deg_data)
    deg_data.close()
    print(deg_dict)

if __name__ == "__main__":
    # add_jobs()
    check_pickle()