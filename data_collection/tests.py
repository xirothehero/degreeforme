from unittest import TestCase, main
import os
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask import Flask, jsonify, request
import json
import pickle

import { get_city_data, pickle_check } from datacollect_p1
import {  } from jobdatacollect_p3


class TestApplication(TestCase):

    def college_city_parse_test(self):
        f = open("college_city_dump.txt", "wb")
        get_city_data(f.write())
        f.close()
        f2 = open("college_city_dump.txt", "rb")
        cities = f2.read()
        assert cities is not None
        f2.close()

    def pickle_test(self):
        pcl = pickle_check()
        assert type(pcl[0]['pay_mid']) is int
        assert type(pcl[50]['degree']) is str

    def job_check(self):
        pcl = open("../api_code/API Data/refined_degree_data3.p", "rb")
        degree_dict = pickle.load(pcl)
        pcl.close()
        for d in degree_dict:
            assert type(d['jobs']) is list
            assert len(d['jobs']) == 2
            print(str({d['degree'] : d['jobs']}))
