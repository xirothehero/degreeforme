import React, { Component } from 'react';
import DegreeCard from '../Components/DegreeCard.js';
import Grid from '@material-ui/core/Grid';
import OverviewContainer from '../Components/OverviewContainer';
import API from '../Assets/api.js';
import Typography from '@material-ui/core/Typography';
import SearchResultsList from '../Components/Search/SearchResultsList.js';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import DegreeSearchBar from '../Components/Search/DegreeSearchBar.js';
import DegreeFilterSortBar from '../Components/Search/DegreeFilterSortBar.js';

class Degree extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      pageCount: 0,
      pageParams: {
        page: 1
      },
      objects: [],
      searchParams: {},
      sortParams: {},
      currQuery: []
    };
  }

  updatePageData = pageNum => {
    let newPageParams = {};

    newPageParams.page = pageNum;

    // Update the state's filter params
    this.setState({pageParams: newPageParams});

    // Add in the other parameters
    let queryParams = {
      ...newPageParams,
      ...this.state.filterSalaryParams,
      ...this.state.searchParams,
      ...this.state.sortParams
    }

    // Load the new data
    this.searchDataFromServer(queryParams);
  }

  searchDataFromServer = params => {
    API.searchDegrees(params).then(json => {
      this.setState({data: json.data, pageCount: json.data['total_pages']})
    })
  }

  // UNUSED
  // loadDataFromServer = params => {
  //   API.getDegrees(params).then(json => {
  //     this.setState({data: json.data, pageCount: json.data['total_pages']})
  //   })
  // }

  updateDegreeSearchData = (query) => {
    // Reset to page 1
    let newSearchParams = {}
    if (query !== "") {
      newSearchParams.keyword = query;
      // Update the current query
      this.setState({currQuery: query.split(" ")});
    } else {
      this.setState({currQuery: []});
    }

    // Update the state's search params
    this.setState({searchParams: newSearchParams});

    let newPageParams = {
      page: 1
    };
    this.setState({pageParams: newPageParams});

    // Add in the filter and sort params
    let queryParams = {
      ...newSearchParams,
      ...this.state.filterSalaryParam,
      ...newPageParams,
      ...this.state.sortParams
    };

    // Load the new data
    this.searchDataFromServer(queryParams);
  }

  updateDegreeSortData = (sort, order) => {
    let newSortParams = {
      sort: sort,
      order: order
    }

    // Update the state's sort params
    this.setState({sortParams: newSortParams});
    let newPageParams = {
      page: 1
    };
    this.setState({pageParams: newPageParams});

    // Add in the search and filter params
    let queryParams = {
      ...newSortParams,
      ...this.state.searchParams,
      ...this.state.filterSalaryParam,
      ...newPageParams
    };

    // Load the new data
    this.searchDataFromServer(queryParams);
  }

  updateSalaryFilterData = (min, max, level) => {
    let newSalaryFilterParams = {}
    if (min !== '') {
      // switch(level) {
      //   case "entry":
      //     newSalaryFilterParams.entryMin = parseInt(min);
      //     break;
      //   case "mid":
      //     newSalaryFilterParams.midMin = parseInt(min);
      //     break;
      //   default:
      //     console.error("Must provide a valid min level.");
      //     break;
      // }
      newSalaryFilterParams.entryMin = parseInt(min);
    }
    if (max !== '') {
      newSalaryFilterParams.entryMax = parseInt(max);
    }
    // Update the state's filter params
    this.setState({filterSalaryParam: newSalaryFilterParams});
    let newPageParams = {
      page: 1
    };
    this.setState({pageParams: newPageParams});

    // Add in the other parameters
    let queryParams = {
      ...newSalaryFilterParams,
      ...this.state.searchParams,
      ...newPageParams,
      ...this.state.sortParams
    }
    // Load the new data
    this.searchDataFromServer(queryParams);
  }

  render() {
    const degrees_data = this.state.data;
    const stopPropagation = (e) => e.stopPropagation();
    const InputWrapper = ({children}) => <div onClick={stopPropagation}>
      {children}
    </div>

    let degree_keyword = "";
    console.log("in degree model page, this.state:", this.state)
    if (typeof(this.state.searchParams.keyword) !== "undefined") {
      degree_keyword = this.state.searchParams.keyword;
    }

    // console.log("in degree model page, this.state.data", this.state.data)
    return (
      <div className="page-body">
        <OverviewContainer updatePageCallback={this.updatePageData.bind(this)} page={this.state.pageParams.page - 1} pageCount={this.state.pageCount}>
          <Typography variant="h4" align="left" style={{'marginTop' : '20px'}}>Degrees</Typography>
          <ExpansionPanel>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <InputWrapper>
                <DegreeSearchBar query={degree_keyword} returnSearch={this.updateDegreeSearchData.bind(this)} onClick={this.clickSummary}/>
              </InputWrapper>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <DegreeFilterSortBar updateDegreeSortData={this.updateDegreeSortData.bind(this)} 
              updateSalaryFilterCallback={this.updateSalaryFilterData.bind(this)}
              />
            </ExpansionPanelDetails>
          </ExpansionPanel>
            {
              degrees_data.length !== 0 && <Grid container="container" spacing={3}>
                {degrees_data.objects.length !== 0 && degrees_data.objects.map(item => <Grid item="item" xs={12} sm={4} key={item.degree}><DegreeCard item={item} query={this.state.objects} key={item.id}/></Grid>)}
              </Grid>
            }
            {this.state.objects.length > 0 && <SearchResultsList id="degreeSearchResultList" data={degrees_data} type="degrees" query={this.state.objects}/>}
        </OverviewContainer>
     </div>
    );
  }
}

  export default Degree;