import React, { Component } from 'react';
import SplashCard from '../Components/SplashCard.js';
import Grid from '@material-ui/core/Grid';
import GlobalSearchBar from '../Components/Search/GlobalSearchBar.js';

// images
import Background from '../Assets/Splash/Main1.jpeg';
import UniversityImage from '../Assets/Splash/University.jpg';
import DegreeImage from '../Assets/Splash/Degree.jpg';
import CityImage from '../Assets/Splash/City.jpg';

const styles = {
  heroImage: {
    backgroundImage: 'linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.5)), url(' + Background + ')',
  }
};

class Splash extends Component {
    render() {
      return (
        <div>
          <div className='heroImage' style={styles.heroImage}>
            <div className='heroText'>
              <h1 className='splashHeader'>Degree For Me</h1>
              <p className='splashText'>Find the right university, degree, and city for you!</p>
              <GlobalSearchBar splashSearch={true}/>
            </div>
          </div>
          <div className="page-body">
            <h1 align="left" style={{color:'#1A535C'}}>Start exploring</h1>
            <Grid container spacing={9}>
              <Grid item xs>
                <SplashCard
                  name='Universities'
                  path='/university'
                  photo={UniversityImage}>
                </SplashCard>
              </Grid>
              <Grid item xs>
                <SplashCard
                  name='Degrees'
                  path='/degree'
                  photo={DegreeImage}>
                </SplashCard>
              </Grid>
              <Grid item xs>
                <SplashCard
                  name='Cities'
                  path='/city'
                  photo={CityImage}>
                </SplashCard>
              </Grid>
            </Grid>
          </div>
        </div>
      );
    }
  }

  export default Splash;
