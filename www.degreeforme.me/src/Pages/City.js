import React, { Component } from 'react';
import CityCard from '../Components/CityCard.js';
import Grid from '@material-ui/core/Grid';
import OverviewContainer from '../Components/OverviewContainer';
import API from '../Assets/api.js';
import Typography from '@material-ui/core/Typography';
import SearchResultsList from '../Components/Search/SearchResultsList.js';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import CitySearchBar from '../Components/Search/CitySearchBar.js';
import CityFilterSortBar from '../Components/Search/CityFilterSortBar.js';

class City extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      pageCount: 0,
      pageParams: {
        page: 1
      },
      objects: [],
      searchParams: {},
      sortParams: {},
      currQuery: []
    };
  }

  updatePageData = pageNum => {
    let newPageParams = {};

    newPageParams.page = pageNum;

    // Update the state's filter params
    this.setState({pageParams: newPageParams});

    // Add in the other parameters
    let queryParams = {
      ...newPageParams,
      ...this.state.filterCostParams,
      ...this.state.searchParams,
      ...this.state.sortParams
    }

    // Load the new data
    this.searchDataFromServer(queryParams);
  }

  searchDataFromServer = params => {
    API.searchCities(params).then(json => {
      this.setState({data: json.data, pageCount: json.data['total_pages']})
    })
  }

  // UNUSED
  // loadDataFromServer = params => {
  //   API.getCities(params).then(json => {
  //     this.setState({data: json.data, pageCount: json.data['total_pages']})
  //   })
  // }

  updateCitySearchData = (query) => {
    // Reset to page 1
    let newSearchParams = {}
    if (query !== "") {
      newSearchParams.keyword = query;
      // Update the current query
      this.setState({currQuery: query.split(" ")});
    } else {
      this.setState({currQuery: []});
    }

    // Update the state's search and location params
    this.setState({searchParams: newSearchParams});
    // this.setState({locationParams: newLocationParams});

    let newPageParams = {
      page: 1
    };
    this.setState({pageParams: newPageParams});

    // Add in the filter and sort params
    let queryParams = {
      ...newSearchParams,
      // ...newLocationParams,
      ...this.state.filterCostParam,
      ...newPageParams,
      ...this.state.sortParams
    };

    // Load the new data
    this.searchDataFromServer(queryParams);
  }

  updateCitySortData = (sort, order) => {
    let newSortParams = {
      sort: sort,
      order: order
    }

    // Update the state's sort params
    this.setState({sortParams: newSortParams});
    let newPageParams = {
      page: 1
    };
    this.setState({pageParams: newPageParams});

    // Add in the search and filter params
    let queryParams = {
      ...newSortParams,
      ...this.state.searchParams,
      ...this.state.filterCostParam,
      ...newPageParams
    };

    // Load the new data
    this.searchDataFromServer(queryParams);
  }

  updateCostFilterData = (min, max) => {
    let newCostFilterParams = {}
    if (min !== '') {
      newCostFilterParams.costMin = parseInt(min);
    }
    if (max !== '') {
      newCostFilterParams.costMax = parseInt(max);
    }
    // Update the state's filter params
    this.setState({filterCostParam: newCostFilterParams});
    let newPageParams = {
      page: 1
    };
    this.setState({pageParams: newPageParams});

    // Add in the other parameters
    let queryParams = {
      ...newCostFilterParams,
      ...this.state.searchParams,
      ...newPageParams,
      ...this.state.sortParams
    }
    // Load the new data
    this.searchDataFromServer(queryParams);
  }

  render() {
    const cities_data = this.state.data;
    const stopPropagation = (e) => e.stopPropagation();
    const InputWrapper = ({children}) => <div onClick={stopPropagation}>
      {children}
    </div>

    let city_keyword = "";
    console.log("in city model page, this.state:", this.state)
    if (typeof(this.state.searchParams.keyword) !== "undefined") {
      city_keyword = this.state.searchParams.keyword;
    }

    return (
      <div className="page-body">
        <OverviewContainer updatePageCallback={this.updatePageData.bind(this)} page={this.state.pageParams.page - 1} pageCount={this.state.pageCount}>
          <Typography variant="h4" align="left" style={{'marginTop' : '20px'}}>Cities</Typography>
          <ExpansionPanel>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <InputWrapper>
                <CitySearchBar query={city_keyword} returnSearch={this.updateCitySearchData.bind(this)} onClick={this.clickSummary}/>
              </InputWrapper>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <CityFilterSortBar updateCitySortData={this.updateCitySortData.bind(this)} 
              updateCostFilterCallback={this.updateCostFilterData.bind(this)}
              />
            </ExpansionPanelDetails>
          </ExpansionPanel>
            {
              cities_data.length !== 0 && <Grid container="container" spacing={3}>
                {cities_data.objects.length !== 0 && cities_data.objects.map(item => <Grid item="item" xs={12} sm={4} key={item.id}><CityCard item={item} query={this.state.sortParams} key={item.id}/></Grid>)}
              </Grid>
            }
            {this.state.objects.length > 0 && <SearchResultsList id="citySearchResultList" data={cities_data} type="cities" query={this.state.objects}/>}
        </OverviewContainer>
     </div>
    );
  }
}

export default City;