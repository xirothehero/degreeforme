import React, { Component } from 'react';
import CardContainer from '../Components/CardContainer.js';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import {format_state_name} from '..//Assets/Helper.js';
import API from '../Assets/api.js';

const styles = {
  bottomDivs: {
    marginTop: '70px'
  }
};

class CityInstance extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      photo: ""
    };
  }

  componentDidMount() {
    const {id} = this.props.match.params;
    // get city data
    API.getCityFromId(id).then(json => {
      this.setState({data: json.data})


      //TODO: photos not implemented yet
      // Try to set the city photo
      if (json.data.photo !== "") {
        this.setState({photo: json.data.photo_url});
      } 
      // else {
      //   // TODO: This probably wont work yet until linking works. If there is no city photo, then get a photo from somewhere
      //   let city_id = json.data.city_id
      //   API.getCity(city_id).then(json => {
      //     let photo_num = Math.floor(Math.random() * 10);
      //     let photo = JSON.parse(json.data.photos[photo_num]);
      //     let photo_url = API.buildCityImageURL(city_id, photo['photo_reference']);
      //     this.setState({photo: photo_url});
      //   });
      // }
    })
  };

  render() {
    let null_message = "No information given";
    console.log("city instance this.state.data: ", this.state.data)
    let states = format_state_name(this.state.data.state);
    let median_home_cost = this.state.data.median_home_cost;
    let grocery = this.state.data.grocery;
    let transportation = this.state.data.transportation;
    let health = this.state.data.health;

    return (<div className="pageColumn">
      <div className="pageContent">
        <CardMedia style={{height:'275px', marginTop:"55px",  borderRadius: "25px"}}
          image={this.state.data.photo_url}
          title="" />
        <Typography id='cityCardName' gutterBottom variant="h5" component="h2" align="left">
            {this.state.data.name}
        </Typography>
        <Typography component="p" variant="caption" align="left">
            State: { states ? states : null_message}
        </Typography>
        <Typography component="p" variant="caption" align="left">
            Median Home Cost: { median_home_cost ? "$" + median_home_cost : null_message}
        </Typography>
        <Typography component="p" variant="caption" align="left">
            Average grocery cost: { grocery ? "$" + grocery : null_message}
        </Typography>
        <Typography component="p" variant="caption" align="left">
            Average transportation cost: { transportation ? "$" + transportation : null_message}
        </Typography>
        <Typography component="p" variant="caption" align="left">
            Average cost for health: { health ? "$" + health : null_message}
        </Typography>


        <div style={styles.bottomDivs}>
          <Typography variant="h6">Popular universities in this city</Typography>
          {this.state.data.college && <CardContainer type="university" uid={this.state.data.college}/>}
        </div>
        <div style={styles.bottomDivs}>
          <Typography variant="h6">Popular degrees in this city</Typography>
          {this.state.data.degree && <CardContainer type="degree" uid={this.state.data.degree}/>}
        </div>
      </div>
    </div>);
  }
}

export default CityInstance;