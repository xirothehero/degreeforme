import React, { Component } from 'react';
import UniversityCard from '../Components/UniversityCard.js';
import Grid from '@material-ui/core/Grid';
import OverviewContainer from '../Components/OverviewContainer';
import API from '../Assets/api.js';
import Typography from '@material-ui/core/Typography';
import SearchResultsList from '../Components/Search/SearchResultsList.js';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import UniversitySearchBar from '../Components/Search/UniversitySearchBar.js';
import UniversityFilterSortBar from '../Components/Search/UniversityFilterSortBar.js';

class University extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      pageCount: 0,
      pageParams: {
        page: 1
      },
      objects: [],
      searchParams: {},
      sortParams: {},
      locationParams: {},
      currQuery: []
    };
  }

  updatePageData = pageNum => {
    let newPageParams = {};

    newPageParams.page = pageNum;

    // Update the state's filter params
    this.setState({pageParams: newPageParams});

    // Add in the other parameters
    let queryParams = {
      ...newPageParams,
      ...this.state.filterSATParams,
      ...this.state.searchParams,
      // ...this.state.locationParams,
      ...this.state.sortParams
    };

    // Load the new data
    this.searchDataFromServer(queryParams);
  }

  // UNUSED
  // loadDataFromServer = params => {
  //   API.getUniversities(params).then(json => {
  //     console.log("in uni load data from server, json:", json)
  //     this.setState({data: json.data, pageCount: json.data['total_pages']})
  //   })
  // }

  searchDataFromServer = params => {
    API.searchUniversities(params).then(json => {
      console.log("in uni search data from server, json:", json)
      this.setState({data: json.data, pageCount: json.data['total_pages']})
    })
  }

  updateUniversitySearchData = (query, place_name, city_id) => {
    // Reset to page 1
    let newSearchParams = {}
    if (query !== "") {
      newSearchParams.keyword = query;
      // Update the current query
      this.setState({currQuery: query.split(" ")});
    } else {
      this.setState({currQuery: []});
    }

    let newLocationParams = {};
    if (city_id !== "") {
      newLocationParams.id = city_id;
      newLocationParams.name = place_name;
    }

    // Update the state's search and location params
    this.setState({searchParams: newSearchParams});
    // this.setState({locationParams: newLocationParams});

    let newPageParams = {
      page: 1
    };
    this.setState({pageParams: newPageParams});

    // Add in the filter and sort params
    let queryParams = {
      ...newSearchParams,
      // ...newLocationParams,
      ...this.state.filterSATParam,
      ...newPageParams,
      ...this.state.sortParams
    };

    // Load the new data
    this.searchDataFromServer(queryParams);
  }

  updateUniversitySortData = (sort, order) => {
    let newSortParams = {
      sort: sort,
      order: order
    }

    // Update the state's sort params
    this.setState({sortParams: newSortParams});
    let newPageParams = {
      page: 1
    };
    this.setState({pageParams: newPageParams});

    // Add in the search and filter params
    let queryParams = {
      ...newSortParams,
      ...this.state.searchParams,
      ...this.state.locationParams,
      ...this.state.filterSATParam,
      ...newPageParams
    };

    // Load the new data
    this.searchDataFromServer(queryParams);
  }

  updateSATFilterData = (unused, sat_score) => {
    let newSATFilterParam = {}
    if (sat_score !== '') {
      newSATFilterParam.sat_score = parseInt(sat_score);
    }
    // Update the state's filter params
    this.setState({filterSATParam: newSATFilterParam});
    let newPageParams = {
      page: 1
    };
    this.setState({pageParams: newPageParams});

    // Add in the other parameters
    let queryParams = {
      ...newSATFilterParam,
      ...this.state.searchParams,
      ...this.state.locationParams,
      // ,
      ...newPageParams,
      ...this.state.sortParams
    }

    console.log("updateSATFilterData, queryParams:", queryParams)
    // Load the new data
    this.searchDataFromServer(queryParams);
  }

  render() {
    const universities_data = this.state.data;
    const stopPropagation = (e) => e.stopPropagation();
    const InputWrapper = ({children}) => <div onClick={stopPropagation}>
      {children}
    </div>

    let university_keyword = "";
    console.log("in university model page, this.state:", this.state)
    if (typeof(this.state.searchParams.keyword) !== "undefined") {
      university_keyword = this.state.searchParams.keyword;
    }
    let place_name = "";
    let city_id = "";
    if (typeof(this.state.locationParams.place_name) !== "undefined" && this.state.locationParams.place_name !== "") {
      place_name = this.state.locationParams.place_name;
      city_id = this.state.locationParams.city_id;
    }

    return (
      <div className="page-body">
        <OverviewContainer updatePageCallback={this.updatePageData.bind(this)} page={this.state.pageParams.page - 1} pageCount={this.state.pageCount}>
          <Typography variant="h4" align="left" style={{'marginTop' : '20px'}}>Universities</Typography>
          <ExpansionPanel>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <InputWrapper>
                <UniversitySearchBar query={university_keyword} value={place_name} city_id={city_id} returnSearch={this.updateUniversitySearchData.bind(this)} onClick={this.clickSummary}/>
              </InputWrapper>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <UniversityFilterSortBar updateUniversitySortData={this.updateUniversitySortData.bind(this)} 
              updateSATFilterCallback={this.updateSATFilterData.bind(this)} 
              // updateDateFilterCallback={this.updateDateFilterData.bind(this)} 
              // updateTimeFilterCallback={this.updateTimeFilterData.bind(this)} 
              // locEnabled={this.props.isGeolocationEnabled}
              />
            </ExpansionPanelDetails>
          </ExpansionPanel>
            {
              universities_data.length !== 0 && <Grid container="container" spacing={3}>
                {universities_data.objects.length !== 0 && universities_data.objects.map(item => <Grid item="item" xs={12} sm={4} key={item.id}><UniversityCard item={item} query={this.state.sortParams} key={item.id}/></Grid>)}
              </Grid>
            }
            {this.state.objects.length > 0 && <SearchResultsList id="universitySearchResultList" data={universities_data} type="universities" query={this.state.objects}/>}
        </OverviewContainer>
     </div>
    );
  }
}

  export default University;