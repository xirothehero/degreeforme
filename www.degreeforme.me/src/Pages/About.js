import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import AboutCard from '../Components/AboutCard.js'
import AboutDataCard from '../Components/AboutDataCard.js'
import axios from 'axios';

class About extends Component {
  constructor(props) {
    super(props);
    this.state = {
      xirothehero: {
                          name:'Ali Kedwaii',
                          quote:'"Sleep is for the weak... and I sleep a lot."',
                          role: "Back-end Developer",
                          degree: "B.S. in Electrical Engineering",
                          commits: 0,
                          issues: 0,
                          unitTests: 11},
      robertyguan: {
                          name:'Courtney Huynh',
                          quote:'"I don\'t have a quote for now."',
                          role:"Front-end Developer",
                          degree: "B.S. in Computer Science",
                          commits: 0,
                          issues: 0,
                          unitTests: 0},
      grexia: {
                          name:'Dheeraj Nuthakki',
                          quote:'"Game of Thrones Season 8 was disappointing."',
                          role:"Front-end Developer",
                          degree: "B.S. in Computer Science",
                          commits: 0,
                          issues: 0,
                          unitTests: 0},                         
      jcmg: {
                          name:'Juan Carlos Moreno',
                          quote:'"Let\'s have fun"',
                          role:"Front-end Developer",
                          degree: "B.S. in Computer Science",
                          commits: 0,
                          issues: 0,
                          unitTests: 2},
      kbush1563: {
                          name:'Kaitlyn Bush',
                          quote:'"I hate AWS with a passion."',
                          role:"Back-end Developer",
                          degree: "B.S. in Computer Science, Minor in Business",
                          commits: 0,
                          issues: 0,
                          unitTests: 0},
      nidhirathod: {
                          name:'Nidhi Rathod',
                          quote:'"Everything happens for a reason."',
                          role:"Front-end Developer",
                          degree: "B.S. in Computer Science",
                          commits: 0,
                          issues: 0,
                          unitTests: 15},
      total: {
                          commits: 0,
                          issues: 0,
                          unitTests: 28},
    };
  }

  componentDidMount() {
    let currentComponent = this;
    const usernames = [
      ['xirothehero', '4153395', 'Ali Kedwaii'],
      ['robertyguan', '4117997', 'Courtney Huynh'],
      ['grexia', '4193535', 'Dheeraj Nuthakki'],
      ['jcmg', '4132019', 'Juan Carlos Moreno'],
      ['kbush1563', '3759123', 'Kaitlyn Bush'],
      ['nidhirathod', '4117443', 'Nidhi Rathod']
    ];

    currentComponent.getAllCommits(1);
    // Get the issues
    usernames.forEach(function(user) {
      // Get the number of issues by using the x-total header
      axios.get('https://gitlab.com/api/v4/projects/13013520/issues?per_page=1&assignee_id=' + user[1]).then(res => {
        let numIssues = Number(res.headers['x-total']);
        let currentUserState = currentComponent.state[user[0]];
        currentUserState.issues = numIssues;

        let currentTotalState = currentComponent.state.total;
        currentTotalState.issues += numIssues;

        let issueUpdate = {};
        issueUpdate[user[0]] = currentUserState;
        issueUpdate.total = currentTotalState;

        currentComponent.setState({issueUpdate});
      });
    });
}

	//get the number of commits
  	async getAllCommits(page) {
        fetch("https://gitlab.com/api/v4/projects/13013520/repository/commits?all=true&per_page=100&page=" + page)
        .then(res => res.json())
        .then((result) => {
            if(Object.entries(result).length !== 0) {
                result.forEach(commit => {
                	this.state.total.commits += 1;
                    switch(commit.committer_name) {
                    	case "Ali Kedwaii":
                        case "xirothehero":
                            this.state.xirothehero.commits += 1;
                            break;
                        case "robertyguan":
                        case "rguan":
                            this.state.robertyguan.commits += 1;
                            break;
                        case "Dheeraj Nuthakki":
                        case "grexia":
                            this.state.grexia.commits += 1;
                            break;
                        case "Juan Carlos Moreno":
                        case "jcmg":
                            this.state.jcmg.commits += 1;
                            break;
                        case "Nidhi Rathod":
                        case "nidhirathod":
                            this.state.nidhirathod.commits += 1;
                            break;
                        case "Kaitlyn Bush":
                        case "kbush1563":
                            this.state.kbush1563.commits += 1;
                            break;
                        default:
                    }
                });
                this.forceUpdate();

                this.getAllCommits(page + 1);
          }
      })
  }

  render() {
    return (
      <div className='pageColumn'>
        <div className='page-body'>
          <Typography variant="h4" align="center">About</Typography>
       	  <Typography variant="body1" align='center'>
            The goal of this project is to provide resources to better understand the impact of degree choice in relation 
            to the job market, cost of tuition, cost of living, and requirements for future retirement options.  This 
            site will provide information about different universities, degrees, and cities. We hope that by providing  
            all these resources together, we can help people better choose what they feel is best for them.
       	  </Typography>
          <Typography component='h1' variant='h6' style={{
          	  color: 'black',
              'fontSize' : '40',
              'marginTop' : '10px',
            }} align='center'>
            <strong>Links</strong><br></br>
            <a href="https://gitlab.com/xirothehero/degreeforme" style={{
                color: '#E4C400',
                'textAlign' : 'center',
                'textDecoration' : 'none'
              }}>
              <strong>GitLab Repo</strong></a><br></br>
            <a href="https://documenter.getpostman.com/view/7931031/S1a4Z84y" style={{
                color: '#E4C400',
                'textAlign' : 'center',
                'textDecoration' : 'none',
                'fontSize' : '10'
              }}>
            <strong>Postman API</strong></a>
          </Typography>
        </div>
       	  <div className='about_content' style={{width:'80%'}}>
            <Grid container spacing={5} justify='center'>
              <Grid item>
                <AboutCard
                  photo={require('../Assets/Bio/Ali.png')}
                  name={this.state['xirothehero']['name']}
                  quote={this.state['xirothehero']['quote']}
                  role={this.state['xirothehero']['role']}
                  degree={this.state['xirothehero']['degree']}
                  link="https://www.linkedin.com/in/alikedwaii/"
                  commits={this.state['xirothehero']['commits']}
                  issues={this.state['xirothehero']['issues']}
                  unitTests={this.state['xirothehero']['unitTests']}>
                </AboutCard>
              </Grid>
              <Grid item>
                <AboutCard
                  photo={require('../Assets/Bio/Courtney.png')}
                  name={this.state['robertyguan']['name']}
                  quote={this.state['robertyguan']['quote']}
                  role={this.state['robertyguan']['role']}
                  degree={this.state['robertyguan']['degree']}
                  link="https://www.linkedin.com/in/qwertyhwin/"
                  commits={this.state['robertyguan']['commits']}
                  issues={this.state['robertyguan']['issues']}
                  unitTests={this.state['robertyguan']['unitTests']}>
                </AboutCard>
              </Grid>
              <Grid item>
                <AboutCard
                  photo={require('../Assets/Bio/Dheeraj.jpg')}
                  name={this.state['grexia']['name']}
                  quote={this.state['grexia']['quote']}
                  link="https://www.linkedin.com/in/dheeraj-nuthakki-156477176"
                  role={this.state['grexia']['role']}
                  degree={this.state['grexia']['degree']}
                  commits={this.state['grexia']['commits']}
                  issues={this.state['grexia']['issues']}
                  unitTests={this.state['grexia']['unitTests']}>
                </AboutCard>
              </Grid>
              <Grid item>
                <AboutCard
                  photo={require('../Assets/Bio/JC.jpg')}
                  name={this.state['jcmg']['name']}
                  quote={this.state['jcmg']['quote']}
                  link="https://www.linkedin.com/in/jcmgiono/"
                  role={this.state['jcmg']['role']}
                  degree={this.state['jcmg']['degree']}
                  commits={this.state['jcmg']['commits']}
                  issues={this.state['jcmg']['issues']}
                  unitTests={this.state['jcmg']['unitTests']}>
                </AboutCard>
              </Grid>
              <Grid item>
                <AboutCard
                  photo={require('../Assets/Bio/Kaitlyn.jpg')}
                  name={this.state['kbush1563']['name']}
                  quote={this.state['kbush1563']['quote']}
                  link="https://www.linkedin.com/in/kaitlyn-bush-0b6753125"
                  role={this.state['kbush1563']['role']}
                  degree={this.state['kbush1563']['degree']}
                  commits={this.state['kbush1563']['commits']}
                  issues={this.state['kbush1563']['issues']}
                  unitTests={this.state['kbush1563']['unitTests']}>
                </AboutCard>
              </Grid>
              <Grid item>
                <AboutCard
                  photo={require('../Assets/Bio/Nidhi.JPG')}
                  name={this.state['nidhirathod']['name']}
                  quote={this.state['nidhirathod']['quote']}
                  role={this.state['nidhirathod']['role']}
                  degree={this.state['nidhirathod']['degree']}
                  link="https://www.linkedin.com/in/nidhirathod/"
                  commits={this.state['nidhirathod']['commits']}
                  issues={this.state['nidhirathod']['issues']}
                  unitTests={this.state['nidhirathod']['unitTests']}>
                </AboutCard>
              </Grid>
            </Grid>
         	</div>
          <div className='page-body'>
            <Typography component='h1' variant='h6' style={{'fontSize' : '40', 'marginTop' : '10px', color: 'black'}} align='center'>Totals</Typography>
            <Typography variant="body1" align='center'>
              <strong>Commits: </strong>{this.state.total.commits}<br></br>
              <strong>Issues: </strong>{this.state.total.issues}<br></br>
              <strong>Unit Tests: </strong>{this.state.total.unitTests}
            </Typography>
          </div>

          <div className='pageContent'>
            <h2 className='about_header' align='left' style={{'marginTop':'100px', 'marginBottom':'5px'}}>
              Data
            </h2>
          <Grid container spacing={5} >
            <Grid item>
              <AboutDataCard
                name='Times Higher Education'
                link='https://www.timeshighereducation.com/student/news/college-degrees-highest-salary-potential#survey-answer'
                photo={require('../Assets/Tools/the.png')}
                alt_text='Times Higher Education Logo'
                description="Used to collect salary info for different types of degrees">
              </AboutDataCard>
            </Grid>
            <Grid item>
              <AboutDataCard
                name='College Scorecard Data'
                link='https://collegescorecard.ed.gov/data/documentation/'
                photo={require('../Assets/Tools/collegescore.png')}
                alt_text='Times Higher Education Logo'
                description="Used to get information on different universities">
              </AboutDataCard>
            </Grid>
            <Grid item>
              <AboutDataCard
                name='City Data'
                link='http://www.city-data.com/'
                photo={require('../Assets/Tools/citydata.png')}
                alt_text='Times Higher Education Logo'
                description="Used to get information on different cities">
              </AboutDataCard>
            </Grid>
            <Grid item>
              <AboutDataCard
                name='Google Images'
                link='https://www.google.com/imghp?hl=en'
                photo={require('../Assets/Tools/google.jpg')}
                alt_text='Google Logo'
                description="Used to get images">
              </AboutDataCard>
            </Grid>
          </Grid>
          </div>
          <div className='pageContent'>
            <h2 className='about_header' align='left' style={{'marginTop':'40px', 'marginBottom':'5px'}}>
              Tools
            </h2>
            <Grid container spacing={5} >
              <Grid item>
                <AboutDataCard
                  name='React'
                  link='https://reactjs.org/'
                  photo={require('../Assets/Tools/react.png')}
                  alt_text='React Logo'
                  description="JavaScript Library for creating user interfaces.">
                </AboutDataCard>
              </Grid>
              <Grid item>
                <AboutDataCard
                  name='Material-UI'
                  link='https://material-ui.com/'
                  photo={require('../Assets/Tools/materialui.svg')}
                  alt_text='Material-UI Logo'
                  description="React components that implement Google's Material Design.">
                </AboutDataCard>
              </Grid>
              <Grid item>
                <AboutDataCard
                  name='NameCheap'
                  link='https://namecheap.com/'
                  photo={require('../Assets/Tools/namecheap.png')}
                  alt_text='NameCheap Logo'
                  description="Domain registrar that offers wide variety of domains.">
                </AboutDataCard>
              </Grid>
              <Grid item>
                <AboutDataCard
                  name='Flask'
                  link='http://flask.pocoo.org/'
                  photo={require('../Assets/Tools/flask.png')}
                  alt_text='Flask Logo'
                  description="Python library that allows for basic web development.">
                </AboutDataCard>
              </Grid>
              <Grid item>
                <AboutDataCard
                  name='Flask-Restless'
                  link='https://flask-restless.readthedocs.io/en/stable/'
                  photo={require('../Assets/Tools/flaskr.png')}
                  alt_text='Flask Restless Logo'
                  description="Provides simple generation of RESTful APIs for database models defined using SQLAlchemy.">
                </AboutDataCard>
              </Grid>
              <Grid item>
                <AboutDataCard
                  name='SQLAlchemy'
                  link='https://www.sqlalchemy.org/'
                  photo={require('../Assets/Tools/sqla.jpg')}
                  alt_text='SQLAlchemy Logo'
                  description="Open-source SQL toolkit and object-relational mapper for Python.">
                </AboutDataCard>
              </Grid>
              <Grid item>
                <AboutDataCard
                  name='MySQL'
                  link='https://www.mysql.com/'
                  photo={require('../Assets/Tools/mysql.png')}
                  alt_text='MySQL Logo'
                  description="Database management system.">
                </AboutDataCard>
              </Grid>
              <Grid item>
                <AboutDataCard
                  name='Gitlab'
                  link='https://about.gitlab.com/'
                  photo={require('../Assets/Tools/gitlab.png')}
                  alt_text='Gitlab Logo'
                  description="A single application for the entire software development lifecycle.">
                </AboutDataCard>
              </Grid>
              <Grid item>
                <AboutDataCard
                  name='Docker'
                  link='https://www.docker.com/'
                  photo={require('../Assets/Tools/docker.png')}
                  alt_text='Docker Logo'
                  description="A tool designed to make it easier to create, deploy, and run applications by using containers.">
                </AboutDataCard>
              </Grid>
              <Grid item>
                <AboutDataCard
                  name='Mocha'
                  link='https://mochajs.org/'
                  photo={require('../Assets/Tools/mocha.svg')}
                  alt_text='Mocha Logo'
                  description="JavaScript test framework used for asynchronous testing.">
                </AboutDataCard>
              </Grid>
              <Grid item>
                <AboutDataCard
                  name='Selenium'
                  link='https://www.seleniumhq.org/'
                  photo={require('../Assets/Tools/selenium.png')}
                  alt_text='Selenium Logo'
                  description="Portable framework for testing web applications.">
                </AboutDataCard>
              </Grid>
              <Grid item>
                <AboutDataCard
                  name='Slack'
                  link='https://slack.com/'
                  photo={require('../Assets/Tools/slack.png')}
                  alt_text='Slack Logo'
                  description="Team collaboration tool used to organize workflow and facilitate project discussion.">
                </AboutDataCard>
              </Grid>
              <Grid item>
                <AboutDataCard
                  name='Postman'
                  link='https://www.getpostman.com/'
                  photo={require('../Assets/Tools/postman.png')}
                  alt_text='Postman Logo'
                  description="API development tool used to test and design APIs.">
                </AboutDataCard>
              </Grid>
              <Grid item>
                <AboutDataCard
                  name='Amazon Web Services'
                  link='https://aws.amazon.com/'
                  photo={require('../Assets/Tools/aws.png')}
                  alt_text='Amazon Web Services Logo'
                  description="Secure cloud computing services platform.">
                </AboutDataCard>
              </Grid>
            </Grid>
          </div>
       	</div>
      );
    }
  }

  export default About;
