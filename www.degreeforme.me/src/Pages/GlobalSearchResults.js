import React, {Component} from 'react';
import Typography from '@material-ui/core/Typography';
import SearchAll from '../Components/Search/GlobalSearchBar.js';
import SearchResultsList from '../Components/Search/SearchResultsList.js';
import OverviewContainer from '../Components/OverviewContainer.js';
import API from '../Assets/api.js';

//icons
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import SwipeableViews from 'react-swipeable-views';

class GlobalSearchResults extends Component {
  constructor(props) {
    super(props);
    let initial_keyword = "";
    if (typeof(this.props.location.state) != "undefined") {
      initial_keyword = this.props.location.state.initialValue;
    } else {
      console.error("location state undefined");
    }

    this.state = {
      university_data: [],
      degree_data: [],
      city_data: [],
      universityPageCount: 0,
      degreePageCount: 0,
      cityPageCount: 0,
      universityTotal: 0,
      degreeTotal: 0,
      cityTotal: 0,
      tabIndex: 0,
      // Populate the initial keyword from whatever was passed from the Splash page
      searchParams: {
        keyword: initial_keyword
      },
      universityPageParams: {
        page: 1
      },
      degreePageParams: {
        page: 1
      },
      cityPageParams: {
        page: 1
      }
    };
  }

  componentDidMount() {
    if (this.state.searchParams.keyword !== "") {
      this.updateGlobalSearchData(this.state.searchParams.keyword);
    }
  }

  updateGlobalSearchData = query => {
    let newSearchParams = {}
    if (query !== "") {
      newSearchParams.keyword = query;
      
      // Update the current query
      this.setState({currQuery: query.split(" ")});
    } else {
      this.setState({currQuery: []});
    }

    // Update the state's search and location params
    this.setState({searchParams: newSearchParams});
    let newPageParams = {
      page: 1
    };
    this.setState({universityPageParams: newPageParams});
    this.setState({degreePageParams: newPageParams});
    this.setState({cityPageParams: newPageParams});

    // Add in the filter and sort params
    let queryParams = {
      ...newSearchParams,
      ...newPageParams
    };

    // Update the university, degree, and city data with the new search query
    this.loadUniversityDataFromServer(queryParams);
    this.loadDegreeDataFromServer(queryParams);
    this.loadCityDataFromServer(queryParams);
  }

  updateUniversityPageAndLoadData = pageNum => {
    let newUniversityPageParams = {};
    newUniversityPageParams.page = pageNum;

    // Update the state
    this.setState({universityPageParams: newUniversityPageParams});

    // Add in the other query parameters
    let queryParams = {
      ...newUniversityPageParams,
      ...this.state.searchParams
    }
    this.loadUniversityDataFromServer(queryParams);
  }

  updateDegreePageAndLoadData = pageNum => {
    let newDegreePageParams = {};
    newDegreePageParams.page = pageNum;

    // Update the state
    this.setState({degreePageParams: newDegreePageParams});

    // Add in the other query parameters
    let queryParams = {
      ...newDegreePageParams,
      ...this.state.searchParams
    }
    this.loadDegreeDataFromServer(queryParams);
  }

  updateCityPageAndLoadData = pageNum => {
    let newCityPageParams = {};
    newCityPageParams.page = pageNum;

    // Update the state
    this.setState({cityPageParams: newCityPageParams});

    // Add in the other query parameters
    let queryParams = {
      ...newCityPageParams,
      ...this.state.searchParams
    }
    this.loadCityDataFromServer(queryParams);
  }

  loadUniversityDataFromServer = params => {
    API.searchUniversities(params).then(json => {
        console.log("load uni data, json:", json)
      this.setState({university_data: json.data, 
        universityPageCount: json.data['total_pages'],
        universityTotal: json.data['num_results']
        });
    });
  }

  loadDegreeDataFromServer = params => {
    // Degrees
    API.searchDegrees(params).then(json => {
      this.setState({degree_data: json.data, degreePageCount: json.data['total_pages'], degreeTotal: json.data['num_results']});
    });
  }

  loadCityDataFromServer = params => {
    // Cities
    API.searchCities(params).then(json => {
      this.setState({city_data: json.data, cityPageCount: json.data['total_pages'], cityTotal: json.data['num_results']});
    });
  }

  handleChange = (university, tabIndex) => {
    this.setState({tabIndex});
  };

  handleChangeIndex = index => {
    this.setState({tabIndex: index});
  };

  render() {
    console.log("in global search res render, this.state: ", this.state)
    return (<div className="page-body">
      <div className='search-body' style={{
          paddingBottom: 20
        }}>
        <SearchAll query={this.state.searchParams.keyword} splashSearch={false} returnSearch={this.updateGlobalSearchData.bind(this)}/>
      </div>
      <Tabs paddingTop="100px" value={this.state.tabIndex} onChange={this.handleChange} indicatorColor="primary" textColor="primary" variant="fullWidth">
        <Tab id="universitySearchTab" label={"UNIVERSITIES: " + this.state.universityTotal + " results"} style={{textTransform: 'capitalize', fontSize:'16px'}}/>
        <Tab id="degreeSearchTab" label={"DEGREES: " + this.state.degreeTotal + " results"} style={{textTransform: 'capitalize', fontSize:'16px'}}/>
        <Tab id="citySearchTab" label={"CITIES: " + this.state.cityTotal + " results"} style={{textTransform: 'capitalize', fontSize:'16px'}}/>
      </Tabs>

      <SwipeableViews axis={'x'} index={this.state.tabIndex} onChangeIndex={this.handleChangeIndex}>
        <TabContainer>
          {
            this.state.universityTotal > 0 && <OverviewContainer updatePageCallback={this.updateUniversityPageAndLoadData.bind(this)} page={this.state.universityPageParams.page - 1} pageCount={this.state.universityPageCount}>
                <SearchResultsList id='globalUniversityResult' data={this.state.university_data} type="universities" query={this.state.currQuery}/>
            </OverviewContainer>
          }
          {this.state.universityTotal === 0 && "No universities found for your query. :("}
        </TabContainer>
        <TabContainer>
          {
            this.state.degreeTotal > 0 && <OverviewContainer updatePageCallback={this.updateDegreePageAndLoadData.bind(this)} page={this.state.degreePageParams.page - 1} pageCount={this.state.degreePageCount}>
                <SearchResultsList id='globalDegreeResult' data={this.state.degree_data} type="degrees" query={this.state.currQuery}/>
              </OverviewContainer>
          }
          {this.state.degreeTotal === 0 && "No degrees found for your query. :("}
        </TabContainer>
        <TabContainer>
          {
            this.state.cityTotal > 0 && <OverviewContainer updatePageCallback={this.updateCityPageAndLoadData.bind(this)} page={this.state.cityPageParams.page - 1} pageCount={this.state.cityPageCount}>
                <SearchResultsList id='globalCityResult' data={this.state.city_data} type="cities" query={this.state.currQuery}/>
              </OverviewContainer>
          }
          {this.state.cityTotal === 0 && "No cities found for your query. :("}
        </TabContainer>
      </SwipeableViews>

    </div>);
  }
}

function TabContainer({children, dir}) {
  return (<Typography component="div" dir={dir} style={{
      padding: 8 * 3
    }}>
    {children}
  </Typography>);
}

export default GlobalSearchResults;
