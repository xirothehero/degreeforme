import React, { Component } from 'react';
import CardContainer from '../Components/CardContainer.js';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import API from '../Assets/api.js';

const styles = {
  bottomDivs: {
    marginTop: '70px'
  }
};

class DegreeInstance extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      photo: ""
    };
  }

  componentDidMount() {
    const {id} = this.props.match.params;
    // get degree data
    API.getDegree(id).then(json => {
      this.setState({data: json.data})

      console.log("in DEGREE instance, data: ",this.state.data)

      // Try to set the degree photo
      if (json.data.photo_url !== "") {
        this.setState({photo: json.data.photo_url});
      } 
      else {
        // TODO this probably doesnt work yet without linking working
        // If there is no degree photo, then get a photo from the city
        // let city_id = json.data.city_id
        // API.getCity(city_id).then(json => {
        //   let photo_num = Math.floor(Math.random() * 10);
        //   let photo = JSON.parse(json.data.photos[photo_num]);
        //   let photo_url = API.buildCityImageURL(city_id, photo['photo_reference']);
        //   this.setState({photo: photo_url});
        // });
      }
    })
  };

  render() {
    let null_message = "No information given";
    let pay_early = this.state.data.pay_early;
    let pay_mid = this.state.data.pay_mid;
    let meaningful_work = this.state.data.meaningful_work;
    let rank = this.state.data.rank;
    let difficulty_choices = ["Easy", "Medium", "Hard"];
    let difficulty = difficulty_choices[Math.floor(Math.random() * 3)];

    return (<div className="pageColumn">
      <div className="pageContent">
        <CardMedia style={{height:'275px', marginTop:"55px",  borderRadius: "25px"}}
          image={this.state.data.photo_url}
          title="" />
        <Typography id='degreeCardName' gutterBottom variant="h5" component="h2" align="left">
            {this.state.data.degree}
        </Typography>
        <Typography component="p" variant="caption" align="left">
            Entry level salary: { pay_early ? "$"+pay_early : null_message}
        </Typography>
        <Typography component="p" variant="caption" align="left">
            Mid-level salary: { pay_mid ? "$"+pay_mid : null_message}
        </Typography>
        <Typography component="p" variant="caption" align="left">
            Future job relation: { meaningful_work ? meaningful_work+"%" : null_message}
        </Typography>
        <Typography component="p" variant="caption" align="left">
            Income ranking: { rank ? rank : null_message}
        </Typography>
        <Typography component="p" variant="caption" align="left">
            Difficulty: {difficulty}
        </Typography>

        
        <div style={styles.bottomDivs}>
          <Typography variant="h6">Popular universities for this degree</Typography>
          {this.state.data.college1 && <CardContainer type="university" uid={this.state.data.college1}/>}
        </div>
        <div style={styles.bottomDivs}>
          <Typography variant="h6">Cities</Typography>
          {this.state.data.city1 && <CardContainer type="city" uid={this.state.data.city1}/>}
        </div>
      </div>
    </div>);
  }
}

export default DegreeInstance;
