import React, { Component } from 'react';
import CardContainer from '../Components/CardContainer.js';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import API from '../Assets/api.js';

const styles = {
  bottomDivs: {
    marginTop: '70px'
  }
};

class UniversityInstance extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      photo: ""
    };
  }

  componentDidMount() {
    const {id} = this.props.match.params;
    // get university data
    API.getUniversity(id).then(json => {
      this.setState({data: json.data})
      console.log("in instance, data: ",this.state.data)


      // Try to set the university photo
      if (json.data.photo_url !== "") {
        this.setState({photo: json.data.photo_url});
      } 
      else {
        // TODO this probably doesnt work yet without linking working
        // If there is no university photo, then get a photo from the city
        // let city_id = json.data.city_id
        // API.getCity(city_id).then(json => {
        //   let photo_num = Math.floor(Math.random() * 10);
        //   let photo = JSON.parse(json.data.photos[photo_num]);
        //   let photo_url = API.buildCityImageURL(city_id, photo['photo_reference']);
        //   this.setState({photo: photo_url});
        // });
      }
    })
  };

  render() {
    let null_message = "No information given";
    let in_state = this.state.data.in_state;
    let out_state = this.state.data.out_of_state;
    let admission_rate = this.state.data.admission_rate;
    let completion = this.state.data.completion;
    let sat_scores = this.state.data.sat_scores;

    return (<div className="pageColumn">
      <div className="pageContent">
        <CardMedia style={{height:'275px', marginTop:"55px",  borderRadius: "25px"}}
          image={this.state.data.photo_url}
          title="" />
        <h1 className='instanceTitle' align='left' style={{'marginTop':'40px'}}>
              {this.state.data.name}
            </h1>
            <Typography component="p" variant="caption" align="left">
                    Average yearly in-state tuition: { in_state ? "$"+in_state : null_message}
                </Typography>
                <Typography component="p" variant="caption" align="left">
                    Average yearly out-of-state tuition: { out_state ? "$"+out_state : null_message}
                </Typography>
                <Typography component="p" variant="caption" align="left">
                    Acceptance rate: { admission_rate ? Math.floor(admission_rate*10000)/100+"%" : null_message}
                </Typography>
                <Typography component="p" variant="caption" align="left">
                    Graduation rate: { completion ? Math.floor(completion*10000)/100+"%" : null_message}
                </Typography>
                <Typography component="p" variant="caption" align="left">
                    Average SAT score: { sat_scores ? sat_scores : null_message}
                </Typography>


        <div style={styles.bottomDivs}>
          <Typography variant="h6">City</Typography>
          {this.state.data.city && <CardContainer name='uni_city_card' type="city" uid={this.state.data.city}/>}
        </div>
        <div style={styles.bottomDivs}>
          <Typography variant="h6">Degrees</Typography>
          {this.state.data.top_program && <CardContainer type="degree" uid={this.state.data.top_program}/>}
        </div>
      </div>
    </div>);
  }
}

export default UniversityInstance;
