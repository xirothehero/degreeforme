import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import UniversityCard from './UniversityCard.js';
import DegreeCard from './DegreeCard.js';
import CityCard from './CityCard.js';
import API from '../Assets/api.js';

class CardContainer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: null
    };
  }

  // will make api call
  componentDidMount() {
    switch (this.props.type) {
      case "university":
        API.getUniversityFromName(this.props.uid).then(json => {
          this.setState({data: json.data.objects[0]})
        })
        break;
      case "city":
        API.getCity(this.props.uid).then(json => {
          // TODO: getting the first object isnt ideal because there may be the same city name but in the wrong state
          this.setState({data: json.data.objects[0]})
        })
        break;
      case "degree":
        API.getDegree(this.props.uid).then(json => {
          this.setState({data: json.data})
        })
        break;
      default:
        break;
    }
  }

  render() {
    let card;

    // if our data has loaded
    if (this.state.data != null) {
      switch (this.props.type) {
        case "university":
          card = <UniversityCard id={this.props.name} item={this.state.data} key={this.props.id}/>
          break;
        case "city":
          card = <CityCard id={this.props.name} item={this.state.data} key={this.props.id}/>
          break;
        case "degree":
          card = <DegreeCard id={this.props.name} item={this.state.data} key={this.props.id}/>
          break;
        default:
          break;
      }
    }

    return (<Grid key={this.props.id}>
      {card}
    </Grid>);
  }
}

export default CardContainer;
