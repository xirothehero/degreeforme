import React from 'react';

import Typography from '@material-ui/core/Typography';

const Footer = () => {
  return (<div className='footer-container'>
    <Typography variant='subtitle2' style={{
        color: 'white',
        paddingBottom: '10px'
      }}>Copyright © 2019 degreeforme.me</Typography>
    <Typography variant='subtitle2' style={{
        color: 'white',
        fontWeight: '400'
      }}>Made at UT Austin</Typography>
  </div>)
}

export default Footer;
