import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { Link } from "react-router-dom";
import API from '../Assets/api.js';

const styles = {
    card: {
      maxWidth: '100%',
    },
    media: {
      height: 250,
    },
  };

class UniversityCard extends Component{
  constructor(props){
    super(props);
    this.state = {
      degree_name: "",      // top program at this university
      city_name: "",        // associated city
      university_photo: ""   // photo of the university
    }
  }

  componentDidMount(){
    // Try to set the photo from the photo, but if it's not present then set a
    // flag to get the university photo
    let need_university_photo = true;
    if (this.props.item.photo_url !== "") {
      need_university_photo = false;
      this.setState({
        university_photo: this.props.item.photo_url  //TODO: figure out if this works/figure out a way to get uni photos
      });
    }

    // Get the associated city
    API.getCity(this.props.item.city).then(json => {
      // Set the city name
      this.setState({
        city_name: json.data.name,
      })
    })

    // Get the associated degree
    API.getDegree(this.props.item.top_program).then(json => {
      // Set the degree name
      this.setState({
        degree_name: json.data.name
      })

      // TODO: this probably doesn't work
      // If we didn't have a university/city photo, get the degree photo
    //   if (need_university_photo) {
    //     let photo_num = Math.floor(Math.random() * 10);
    //     let photo = JSON.parse(json.data.photos[photo_num]);
    //     let photo_url = API.buildDegreeImageURL(this.props.item.degree_id, photo['photo_reference']);
    //     this.setState({
    //       university_photo: photo_url
    //     })
    //   }
    })
  };
  render (){
    let null_message = "No information given";
    let in_state = this.props.item.in_state;
    let out_state = this.props.item.out_of_state;
    let admission_rate = this.props.item.admission_rate;
    let completion = this.props.item.completion;
    let sat_scores = this.props.item.sat_scores;
    if(this.props.query) {
      var sort_query = this.props.query.sort;
      var sort_data = this.props.item[sort_query];
    }
    

    // console.log("in uni card, this.props:", this.props.item)
    return(
      <Card className={this.props.classes.card} id={this.props.id}>
        <CardActionArea component={Link} to={"/university/" + this.props.item.id}>
          <CardMedia
            className={this.props.classes.media}
            image={this.state.university_photo}
            title="">
          </CardMedia>
            <CardContent style={{maxHeight: 'auto', overflow:'hidden'}}>
                <Typography id='universityCardName' gutterBottom variant="h5" component="h2" align="left">
                    {this.props.item.name}
                </Typography>
                <Typography component="p" variant="caption" align="left">
                    Average yearly in-state tuition: { in_state ? "$"+in_state : null_message}
                </Typography>
                <Typography component="p" variant="caption" align="left">
                    Average yearly out-of-state tuition: { out_state ? "$"+out_state : null_message}
                </Typography>
                <Typography component="p" variant="caption" align="left">
                    Acceptance rate: { admission_rate ? Math.floor(admission_rate*10000)/100+"%" : null_message}
                </Typography>
                <Typography component="p" variant="caption" align="left">
                    Graduation rate: { completion ? Math.floor(completion*10000)/100+"%" : null_message}
                </Typography>
                <Typography component="p" variant="caption" align="left">
                    Average SAT score: { sat_scores ? sat_scores : null_message}
                </Typography>
                {sort_query &&
                  <Typography component="p" variant="caption" align="left">
                    {sort_query}: {sort_data}
                  </Typography>
                }
            </CardContent>
        </CardActionArea>
      </Card>
    )
  }
}
UniversityCard.propTypes = {
    classes: PropTypes.object.isRequired,
  };
export default withStyles(styles)(UniversityCard);
