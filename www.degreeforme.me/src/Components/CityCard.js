import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';		

// material-ui imports
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import API from '../Assets/api.js';
import {format_state_name} from '..//Assets/Helper.js';

const styles = {
    card: {
      maxWidth: '100%',
    },
    media: {
      height: 250,
    },
  };

class CityCard extends Component{
  constructor(props){
    super(props);
    this.state = {
      city_name: "",                // associated cities
      city_photo: "",               // photo representing this field
      state_name: "",               // state the city is in TODO: figure out how to reuse this val w/o formatting each time
    }
  }

  componentDidMount(){
    if (this.props.item.photo_url !== "") {
      this.setState({
        city_photo: this.props.item.photo_url
      });
    }

    // Get the associated city
    API.getCity(this.props.item.city).then(json => {
      // Set the city name
      this.setState({
        city_name: json.data.name,
      })
    })
  };

  
  render (){
    let null_message = "No information given";
    let states = format_state_name(this.props.item.state);
    let population = this.props.item.population;
    let housing = this.props.item.housing;
    let median_income = this.props.item.median_income;
    let median_age = this.props.item.median_age;
    let overall = this.props.item.overall;
    console.log("in city card, this.props:", this.props)

    return(
      <Card className={this.props.classes.card} id={this.props.id}>
        <CardActionArea component={Link} to={"/city/" + this.props.item.id}>
          <CardMedia
            className={this.props.classes.media}
            image={this.state.city_photo}
            title="">
          </CardMedia>
            <CardContent style={{maxHeight: 'auto', overflow:'hidden'}}>
              <Typography id='cityCardName' gutterBottom variant="h5" component="h2" align="left">
                    {this.props.item.name}
                </Typography>
                <Typography component="p" variant="caption" align="left">
                    <strong>State: </strong>{ states ? states : null_message}
                </Typography>
                <Typography component="p" variant="caption" align="left">
                    <strong>Population: </strong>{ population ? population : null_message}
                </Typography>
                <Typography component="p" variant="caption" align="left">
                    <strong>Yearly housing cost: </strong>{ housing ? "$" + housing : null_message}
                </Typography>
                <Typography component="p" variant="caption" align="left">
                    <strong>Median income: </strong>{ median_income ? "$" + median_income : null_message}
                </Typography>
                <Typography component="p" variant="caption" align="left">
                    <strong>Median age: </strong>{ median_age ? median_age : null_message}
                </Typography>
                <Typography component="p" variant="caption" align="left">
                    <strong>Overall cost: </strong>{ overall ? (overall/100)+"x the national average" : null_message}
                </Typography>

            </CardContent>

        </CardActionArea>
      </Card>
    )
  }
}
CityCard.propTypes = {
    classes: PropTypes.object.isRequired,
  };
export default withStyles(styles)(CityCard);
