// Calendar to pick starting and end dates
// TODO: replace with a price slider?


// import React from 'react';
// import PropTypes from 'prop-types';
// import Grid from '@material-ui/core/Grid';
// import {withStyles} from '@material-ui/core/styles';

// const styles = {
//   grid: {
//     width: '90%'
//   },
//   label: {
//     fontWeight: '600',
//     alignItems: 'left',
//     color: '#494B4E',
//     paddingTop: 15
//   }
// };

// class MaterialUIPickers extends React.Component {
//   state = {
//     today: new Date().getTime(),
//     minDate: new Date().getTime(),
//     maxDate: null,
//     returnFilter: this.props.returnFilter
//   };

//   handleMinDateChange = date => {
//     this.setState({minDate: date});
//     this.handleDateChange(date, this.state.maxDate);
//   };

//   handleMaxDateChange = date => {
//     this.setState({maxDate: date});
//     this.handleDateChange(this.state.minDate, date);
//   };

//   handleDateChange = (start, end) => {
//     if (start === null) {
//       this.setState({minDate: this.state.today});
//       start = this.state.today;
//     }
//     this.state.returnFilter(start, end);
//   };

  
  // render() {
  //   const {classes} = this.props;
  //   const {minDate, maxDate} = this.state;
    
    // return (
      
    //   <MuiPickersUtilsProvider utils={MomentUtils}>
    //     <div className={classes.grid} style={{'alignItems':'left'}}>
    //       <Grid container item spacing={24}>
    //         <Grid item>
    //           <DatePicker
    //             keyboard
    //             clearable
    //             clearLabel="Today"
    //             margin="normal"
    //             label="From"
    //             value={minDate}
    //             onChange={this.handleMinDateChange}
    //             format="MM/DD/YYYY"
    //             minDate={new Date()}
    //             style={{'width':'150px', 'marginTop':'0'}}
    //             mask={[/\d/, /\d/, "/", /\d/, /\d/, "/", /\d/, /\d/, /\d/, /\d/]}
    //           />
    //         </Grid>
    //         <Grid item>
    //           <DatePicker
    //             keyboard
    //             clearable
    //             margin="normal"
    //             label="To"
    //             value={maxDate}
    //             onChange={this.handleMaxDateChange}
    //             format="MM/DD/YYYY"
    //             minDate={minDate === null ? new Date() : minDate}
    //             style={{'width':'150px', 'marginTop':'0'}}
    //           />
    //         </Grid>
    //       </Grid>
    //   </div>
    // </MuiPickersUtilsProvider>);
//   }
// }

// MaterialUIPickers.propTypes = {
//   classes: PropTypes.object.isRequired
// };

// export default withStyles(styles)(MaterialUIPickers);
