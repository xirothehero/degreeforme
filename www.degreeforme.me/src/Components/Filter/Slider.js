import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import noUiSlider from 'nouislider';

// var slider = document.getElementById('slider');

// noUiSlider.create(slider, {
//     start: [20, 80],
//     connect: true,
//     range: {
//         'min': 0,
//         'max': 100
//     }
// });

// TODO: copied from MinMaxInput.js, no styles yet
const styles = theme => ({
    container: {
      display: 'flex',
      flexWrap: 'wrap'
    },
    textField: {
      marginRight: theme.spacing.unit,
      width: 90
    },
    dense: {
      marginTop: 16
    },
    menu: {
      width: 200
    }
  });

class Slider extends React.Component {
    state = {
        min: '',
        max: '',
        returnFilter: this.props.returnFilter
    };
  
    handleChange = name => event => {
      var newValue = event.target.value;
      if (newValue < 0) {
        newValue = 0;
      }
      this.setState({[name]: newValue});
      let min_max = {
        min: this.state.min,
        max: this.state.max
      };
      min_max[name] = newValue;
      // If both a min and max are set and min > max, then don't do anything
      // if (min_max.min !== '' && min_max.max !== '' && parseInt(min_max.min) >) {
      //   return;
      // }
      // Pass the values to the callback
      this.state.returnFilter(min_max.min, min_max.max);
    };
  
    render() {
      const {classes} = this.props;
      var slider = document.getElementById('slider-format');
      console.log("in slider.js, slider:", slider)
      noUiSlider.create(slider, {
          start: [20, 80],
          connect: true,
          step: 1,
          orientation: 'horizontal', // 'horizontal' or 'vertical'
          range: {
              'min': 0,
              'max': 100
          },
          format: {
              from: function(value) {
                      return parseInt(value);
                  },
              to: function(value) {
                      return parseInt(value);
                  }
              }
      //  format: wNumb({
      //    decimals: 0
      //  })
      
      });
      return (<form className={classes.container} noValidate="noValidate" autoComplete="off"></form>);
    }
  }
  
  Slider.propTypes = {
    classes: PropTypes.object.isRequired
  };
  
  export default withStyles(styles)(Slider);
     