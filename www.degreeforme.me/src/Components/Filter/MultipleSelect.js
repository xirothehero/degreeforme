import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
import API from '../../config/api.js'

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 150,
    maxWidth: 400
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  chip: {
    margin: theme.spacing.unit / 4
  },
  noLabel: {
    marginTop: theme.spacing.unit * 3
  }
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250
    }
  }
};

function getStyles(category, that) {
  return {
    fontWeight: that.state.category.indexOf(category) === -1
      ? that.props.theme.typography.fontWeightRegular
      : that.props.theme.typography.fontWeightMedium
  };
}

class MultipleSelect extends React.Component {
  state = {
    category: [],
    categories: []
  };

  handleChange = event => {
    this.setState({category: event.target.value});
    let str = event.target.value.join(",");
    this.props.updateSelection(str);
  };

  componentDidMount() {
    // Retrieve and save the space names
    API.getGroupCategories().then(json => {
      this.setState({categories: json.data})
    })
  }

  render() {
    const {classes} = this.props;

    return (<div className={classes.root}>
      <FormControl className={classes.formControl}>
        <InputLabel htmlFor="select-multiple-chip">Category</InputLabel>
        <Select multiple="multiple" value={this.state.category} onChange={this.handleChange} input={<Input id = "select-multiple-chip" />} renderValue={selected => (<div className={classes.chips}>
            {selected.map(value => (<Chip key={value} label={value} className={classes.chip}/>))}
          </div>)} MenuProps={MenuProps}>
          {
            this.state.categories.map(category => (<MenuItem key={category} value={category} style={getStyles(category, this)}>
              {category}
            </MenuItem>))
          }
        </Select>
      </FormControl>
    </div>);
  }
}

MultipleSelect.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles, {withTheme: true})(MultipleSelect);
