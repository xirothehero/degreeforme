import React, {Component} from 'react'
import PropTypes from "prop-types";

import Button from '@material-ui/core/Button';

class OpenNowButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isToggleOn: false,
      bgColor: 'white',
      textColor: '#494B4E',
      outline: '1px solid #494B4E'
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    if (!this.state.isToggleOn) {
      this.setState(state => ({
        isToggleOn: !state.isToggleOn,
        bgColor: '#1BC86C',
        textColor: 'white',
        outline: '1px solid #1BC86C'
      }));
      this.props.updateOpenNow(true)
    } else {
      this.setState(state => ({
        isToggleOn: !state.isToggleOn,
        bgColor: 'white',
        textColor: '#494B4E',
        outline: '1px solid #494B4E'
      }));
      this.props.updateOpenNow(false)
    }
  }

  render() {
    return (<Button variant='outlined' className='openButton' style={{
        backgroundColor: this.state.bgColor,
        color: this.state.textColor,
        border: this.state.outline
      }} onClick={this.handleClick}>
      Open Now
    </Button>)
  }
}

OpenNowButton.propTypes = {
  classes: PropTypes.object.isRequired
};

export default OpenNowButton;
