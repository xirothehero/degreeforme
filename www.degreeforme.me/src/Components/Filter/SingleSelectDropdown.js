import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    minWidth: 150,
    maxWidth: 400
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  }
});

class SingleSelectDropdown extends React.Component {
  state = {
    rating: '',
    name: 'Rating',
    labelWidth: 0
  };

  componentDidMount() {
    this.setState({
      labelWidth: ReactDOM.findDOMNode(this.InputLabelRef).offsetWidth
    });
  }

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
    this.props.updateSelection(event.target.value.value)
  };

  render() {
    const {classes} = this.props;

    return (<form className={classes.root} autoComplete="off">
      <FormControl variant="outlined" className={classes.formControl}>
        <InputLabel ref={ref => {
            this.InputLabelRef = ref;
          }} htmlFor="outlined-rating-simple">
          {this.props.name}
        </InputLabel>
        <Select value={this.state.rating} onChange={this.handleChange} input={<OutlinedInput
          labelWidth = {
            this.state.labelWidth
          }
          name = "rating"
          id = "outlined-rating-simple"
          />}>
          <MenuItem value={{
              value: 'none'
            }}>
            <em>None</em>
          </MenuItem>
          {
            this.props.list.map(category => (<MenuItem value={category}>
              {category.name}
            </MenuItem>))
          }
        </Select>
      </FormControl>
    </form>);
  }
}

SingleSelectDropdown.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SingleSelectDropdown);
