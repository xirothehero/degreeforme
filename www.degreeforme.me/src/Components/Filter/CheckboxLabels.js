import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import Checkbox from '@material-ui/core/Checkbox';

const styles = {
  root: {
    '&$checked': {
      color: '#1BC86C'
    },
    paddingRight: 5
  },
  checked: {}
};

class CheckboxLabels extends React.Component {
  constructor(props) {
    super(props);

    let checkedItems = new Map();
    for (var label in this.props.checklist) {
      checkedItems.set(this.props.checklist[label].value, false);
    }

    this.state = {
      checkedItems: checkedItems,
      checklist: this.props.checklist
    }
  }

  handleChange = e => {
    const item = e.target.defaultValue;
    const isChecked = e.target.checked;
    let newCheckedItems = this.state.checkedItems;
    newCheckedItems.set(item, isChecked);
    this.setState({checkedItems: newCheckedItems});

    // Call the callback to update the filtering
    this.props.returnFilter(newCheckedItems.get(this.props.checklist[0].value), newCheckedItems.get(this.props.checklist[1].value), newCheckedItems.get(this.props.checklist[2].value));
  }

  render() {
    const {classes} = this.props;

    return (<React.Fragment>
      {
        this.props.checklist.map(item => (<FormControlLabel control={<Checkbox
          // checked={this.state.checkedItems.get(item.label)}
          onChange = {
            this.handleChange
          }
          value = {
            item.value
          }
          label = {
            item.label
          }
          classes = {{
                    root: classes.root,
                    checked: classes.checked,
                  }}
          />} label={item.label}/>))
      }
    </React.Fragment>);
  }
}

CheckboxLabels.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CheckboxLabels);
