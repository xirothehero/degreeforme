import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  textField: {
    marginRight: theme.spacing.unit,
    width: 90
  },
  dense: {
    marginTop: 16
  },
  menu: {
    width: 200
  }
});


class MinMaxInput extends React.Component {
  state = {
    min: '',
    max: '',
    returnFilter: this.props.returnFilter
  };

  handleChange = name => event => {
    var newValue = event.target.value;
    if (newValue < 0) {
      newValue = 0;
    }
    this.setState({[name]: newValue});
    let min_max = {
      min: this.state.min,
      max: this.state.max
    };
    min_max[name] = newValue;
    // If both a min and max are set and min > max, then don't do anything
    if (min_max.min !== '' && min_max.max !== '' && parseInt(min_max.min) > parseInt(min_max.max)) {
      return;
    }
    // Pass the values to the callback
    this.state.returnFilter(min_max.min, min_max.max);
  };

  render() {
    const {classes} = this.props;
    var query = this.props.query;
    switch(query) {
      case "sat":
        return (
        <form className={classes.container} noValidate="noValidate" autoComplete="off">
        <TextField id="outlined-number" label="Your Score" value={this.state.max} onChange={this.handleChange('max')} type="number" className={classes.textField} InputLabelProps={{
            shrink: true
          }} margin="normal" variant="outlined"/>
        </form>);
  
      case "salary":
      case "cost":
        return (
        <form className={classes.container} noValidate="noValidate" autoComplete="off">
        <TextField id="outlined-number" label="Min" value={this.state.min} onChange={this.handleChange('min')} type="number" className={classes.textField} InputLabelProps={{
            shrink: true
          }} margin="normal" variant="outlined"/>
        <TextField id="outlined-number" label="Max" value={this.state.max} onChange={this.handleChange('max')} type="number" className={classes.textField} InputLabelProps={{
            shrink: true
          }} margin="normal" variant="outlined"/>
        </form>);
  
      default:
        console.error("Must provide valid query to MinMaxInput.");
        break;
    }
  }
}

MinMaxInput.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(MinMaxInput);
