import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

// material-ui imports
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import API from '../Assets/api.js';

const styles = {
    card: {
      maxWidth: '100%',
    },
    media: {
      height: 250,
    },
  };

class DegreeCard extends Component{
  constructor(props){
    super(props);
    this.state = {
      top_universities_names: "",   // names of top universities for this degree
      city_name: "",                // associated cities
      degree_photo: ""              // photo representing this field
    }
  }

  componentDidMount(){
    // Try to set the photo from the photo, but if it's not present then set a
    // flag to get the degree photo
    let need_degree_photo = true;
    if (this.props.item.photo_url !== "") {
      need_degree_photo = false;
      this.setState({
        degree_photo: this.props.item.photo_url
      });
    }

    // Get the associated city
    API.getCity(this.props.item.city).then(json => {
      // Set the city name
      this.setState({
        city_name: json.data.name,
      })
    })

    // Get the associated university
    API.getUniversity(this.props.item.top_program).then(json => {
      // Set the university name
      this.setState({
        top_universities_names: json.data.name
      })
    })
  };

  
  render (){
    console.log("IN DEGREE CARD, this.props: ", this.props);
    let null_message = "No information given";
    let pay_early = this.props.item.pay_early;
    let pay_mid = this.props.item.pay_mid;
    let meaningful_work = this.props.item.meaningful_work;
    let rank = this.props.item.rank;
    let job1 = this.props.item.job1;
    let job2 = this.props.item.job2;

    return(
      <Card className={this.props.classes.card} id={this.props.id}>
        <CardActionArea component={Link} to={"/degree/" + this.props.item.degree}>
          <CardMedia
            className={this.props.classes.media}
            image={this.state.degree_photo}
            title="">
          </CardMedia>
            <CardContent style={{maxHeight: 'auto', overflow:'hidden'}}>
              <Typography id='degreeCardName' gutterBottom variant="h5" component="h2" align="left">
                  {this.props.item.degree}
              </Typography>
              <Typography component="p" variant="caption" align="left">
                  Entry level salary: { pay_early ? "$"+pay_early : null_message}
              </Typography>
              <Typography component="p" variant="caption" align="left">
                  Mid-level salary: { pay_mid ? "$"+pay_mid : null_message}
              </Typography>
              <Typography component="p" variant="caption" align="left">
                  Future job relation: { meaningful_work ? meaningful_work+"%" : null_message}
              </Typography>
              <Typography component="p" variant="caption" align="left">
                  Income ranking: { rank ? rank : null_message}
              </Typography>
              <Typography component="p" variant="caption" align="left">
                  Possible careers: { job1 && job2 ? job1+", "+job2 : null_message}
              </Typography>
            </CardContent>

        </CardActionArea>
      </Card>
    )
  }
}
DegreeCard.propTypes = {
    classes: PropTypes.object.isRequired,
  };
export default withStyles(styles)(DegreeCard);
