import React, { Component } from 'react';
import ReactPaginate from 'react-paginate';

class OverviewContainer extends Component {

    componentDidMount() {
      this.props.updatePageCallback(1);
      // this.props.loadDataFromServer(1);
    }

    handlePageClick = data => {
      console.log("in overview container, data: ",data)
      let selected = data.selected;
      this.props.updatePageCallback(selected + 1);
      // this.props.loadDataFromServer(selected+1);
      window.scrollTo(0, 0);
    };

    render() {
      return (
        <div>
          {this.props.children}
          <div className='tableDiv'>
            <ReactPaginate
              previousLabel={'prev'}
              nextLabel={'next'}
              breakLabel={'...'}
              breakClassName={'break-me'}
              pageCount={parseInt(this.props.pageCount)}
              marginPagesDisplayed={1}
              pageRangeDisplayed={2}
              onPageChange={this.handlePageClick}
              containerClassName={'pagination'}
              pageClassName={'paginationPage'}
              activeClassName={'paginationActive'}
              pageLinkClassName={'paginationA'}
            />
          </div>
        </div>
      );
    }
  }

  export default OverviewContainer;
