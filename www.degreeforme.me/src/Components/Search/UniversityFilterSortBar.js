import React, { Component } from 'react'
import PropTypes from "prop-types";

import Sort from './Sort.js';
import Typography from '@material-ui/core/Typography';

import MaterialUIPickers from '../Filter/MaterialUIPickers.js';
import MinMaxInput from '../Filter/MinMaxInput.js';
import CheckboxLabels from '../Filter/CheckboxLabels.js';
import noUiSlider from '../Filter/Slider.js';

const universityTimes = [
  { label: 'Morning', value: 'checkedMorning'},
  { label: 'Afternoon', value: 'checkedAfternoon'},
  { label: 'Night', value: 'checkedNight'}
];

const demographics = ["asian", "asian_pacific_islander", "black", "hispanic", "white"];

function formatDemographic(name) {
    if(name) {
        name = name.toString().replace(/_/g, " "); // replace underscores with spaces
        name = name.charAt(0).toUpperCase() + name.slice(1);
        return name;
    }
}

function getChoices() {
    let choices = [
        { value: 'name', displayName: 'Name', defOrder: 'asc'},
        { value: 'in_state', displayName: 'Tuition in state', defOrder: 'asc'},
        { value: 'out_of_state', displayName: 'Tuition out of state', defOrder: 'asc'},
        { value: 'admission_rate', displayName: 'Acceptance rate', defOrder: 'desc'},
        { value: 'sat_scores', displayName: 'Avg SAT score', defOrder: 'asc'},
        { value: 'completion', displayName: 'Graduation rate', defOrder: 'desc'}
      ];
    demographics.forEach(element => {
        choices.push({ value: element, displayName: formatDemographic(element), defOrder: 'desc'});
    });
    return choices;
};


const styles = {
  filterText: {
      'fontWeight': 600,
  },
  coloredText: {
      'fontWeight': 600,
      'color': '#E4C400' // accent color
  }
};

class UniversityFilterSortBar extends Component {
  render() {
    return (
      <div className="filter-sort-container">
        <div className="filter-body">
          <Typography variant='subtitle1' align='left' style={styles.coloredText}>Filter by</Typography>
          {/* <div className="filter-item">
            <Typography variant='subtitle2' align='left' style={styles.filterText}>Tuition</Typography>
            TODO: implement a slider here
          </div>
          <div className="filter-item">
            <Typography variant='subtitle2' align='left' style={styles.filterText}>Location</Typography>
            TODO: implement some way to filter by state here
          </div> */}
          <div className="filter-item">
            <Typography variant='subtitle2' align='left' style={styles.filterText}>Average SAT score</Typography>
            <MinMaxInput query="sat" returnFilter={this.props.updateSATFilterCallback.bind(this)}/>
          </div>
        </div>
        <div className="sort-body">
          <Typography variant='subtitle1' align='left' style={styles.coloredText}>Sort by</Typography>
          <Sort options={getChoices()} updateSortData={this.props.updateUniversitySortData.bind(this)}/>
        </div>
      </div>
    )
  }
}

UniversityFilterSortBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default UniversityFilterSortBar;
