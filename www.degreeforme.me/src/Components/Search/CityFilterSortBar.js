import React, { Component } from 'react'
import PropTypes from "prop-types";
import Sort from './Sort.js';
import Typography from '@material-ui/core/Typography';
import MinMaxInput from '../Filter/MinMaxInput.js';
import noUiSlider from '../Filter/Slider.js';

function getChoices() {
    let choices = [
        { value: 'name', displayName: 'Name', defOrder: 'asc'},
        { value: 'median_income', displayName: 'Median income', defOrder: 'asc'},
        { value: 'median_age', displayName: 'Median age', defOrder: 'asc'},
        { value: 'population', displayName: 'Population', defOrder: 'desc'},
        { value: 'overall', displayName: 'Overall cost', defOrder: 'desc'}
      ];
    return choices;
};


const styles = {
  filterText: {
      'fontWeight': 600,
  },
  coloredText: {
      'fontWeight': 600,
      'color': '#E4C400' // accent color
  }
};

class CityFilterSortBar extends Component {
  render() {
    return (
      <div className="filter-sort-container">
        <div className="filter-body">
          <Typography variant='subtitle1' align='left' style={styles.coloredText}>Filter by</Typography>
          <div className="filter-item">
            <Typography variant='subtitle2' align='left' style={styles.filterText}>Median yearly housing cost</Typography>
            <MinMaxInput query="cost" returnFilter={this.props.updateCostFilterCallback.bind(this)}/>
          </div>
        </div>
        <div className="sort-body">
          <Typography variant='subtitle1' align='left' style={styles.coloredText}>Sort by</Typography>
          <Sort options={getChoices()} updateSortData={this.props.updateCitySortData.bind(this)}/>
        </div>
      </div>
    )
  }
}

CityFilterSortBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default CityFilterSortBar;
