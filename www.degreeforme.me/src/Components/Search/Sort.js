import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';

import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  formControl: {
    margin: 2,
    width: 160,
    display: 'flex'
  },
  selectEmpty: {
    marginTop: 10
  },
  root: {
    borderRadius: 4,
    color: '#E4C400', // accent color
    border: '1px solid #E4C400', // accent color
    height: 55,
    width: 50
  }
});

class Sort extends React.Component {
  state = {
    sort: this.props.options[0],
    open: false,
    labelWidth: 0,
    order: this.props.options[0].defOrder
  };

  
  componentDidMount() {
    this.setState({
      labelWidth: ReactDOM.findDOMNode(this.InputLabelRef).offsetWidth
    });
  }

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value,
      sort: event.target.value,
      order: event.target.value.defOrder
    });
    this.props.updateSortData(event.target.value.value, event.target.value.defOrder)
  };

  handleClose = () => {
    this.setState({open: false});
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  onButtonClick = () => {
    if (this.state.order === 'asc') {
      this.setState({order: 'desc'});
      this.props.updateSortData(this.state.sort.value, 'desc')
    } else {
      this.setState({order: 'asc'});
      this.props.updateSortData(this.state.sort.value, 'asc')
    }
  };

  render() {
      
    console.log("in sort.js, this.props:", this.props);
    const {classes} = this.props;

    return (<form autoComplete="off" className="form-container">
      <div className="sort-button">
        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel ref={ref => {
              this.InputLabelRef = ref;
            }} htmlFor="outlined-age-simple">
            Sort By
          </InputLabel>
          <Select value={this.state.sort} onChange={this.handleChange} input={<OutlinedInput
            labelWidth = {
              this.state.labelWidth
            }
            name = "sort"
            id = "outlined-age-simple"
            />}>
            {
              this.props.options.map(option => (<MenuItem value={option} disabled={option.displayName === "Distance" && !this.props.locEnabled}>
                {option.displayName}
              </MenuItem>))
            }
          </Select>
        </FormControl>
      </div>
      <div className="order-button">
        <Button classes={{
            root: classes.root
          }} variant="outlined" onClick={this.onButtonClick} disabled={this.state.isDisabled} style={styles.button}>
          {
            this.state.order === 'asc'
              ? "Asc"
              : "Desc"
          }
        </Button>
      </div>
    </form>);
  }
}

Sort.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Sort);
