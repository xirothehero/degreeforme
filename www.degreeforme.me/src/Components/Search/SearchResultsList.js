import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import UniversitySearchResultCard from './UniversitySearchResultCard.js';
import DegreeSearchResultCard from './DegreeSearchResultCard.js';
import CitySearchResultCard from './CitySearchResultCard.js';

const styles = {
  card: {
    maxWidth: '100%'
  },
  searchList: {
    'list-style-type': 'none',
    'padding-left': 0
  },
  searchResult: {
    'margin': '10px'
  }

};

const SearchResultsList = (props) => {
  console.log("in search res list, props:", props)
  switch (props.type) {
    case "universities":
      return (<ul id={props.id} className={props.classes.searchList}>
        {props.data.objects.map(item => <li className={props.classes.searchResult}><UniversitySearchResultCard id='universityResultCard' item={item} query={props.query} key={item.id}/></li>)}
      </ul>)
    case "degrees":
      return (<ul id={props.id} className={props.classes.searchList}>
        {props.data.objects.map(item => <li className={props.classes.searchResult}><DegreeSearchResultCard id='degreeResultCard' item={item} query={props.query} key={item.degree}/></li>)}
      </ul>)
    case "cities":
      return (<ul id={props.id} className={props.classes.searchList}>
        {props.data.objects.map(item => <li className={props.classes.searchResult}><CitySearchResultCard id='citySearchCard' item={item} query={props.query} key={item.id}/></li>)}
      </ul>)
    default:
      console.error('Unknown search list type. Using default');
      return;
  }

}

SearchResultsList.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SearchResultsList);
