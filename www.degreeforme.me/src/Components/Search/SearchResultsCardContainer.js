import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import UniversityCard from '../UniversityCard.js';
import DegreeCard from '../DegreeCard.js';
import CityCard from '../CityCard.js';

class SearchResultsCardContainer extends Component {

  constructor(props) {
    super(props);

    this.state = {
      data: null,
      userLocation: {
        accuracy: 0,
        altitude: 0,
        altitudeAccuracy: 0,
        heading: 0,
        latitude: 30.2862725,
        longitude: -97.73667119999999,
        speed: 0
      }
    };
  }

  render() {
    let card;
    let cur_id;
    console.log("in search res card contain, this.props:", this.props);
    if (this.props.item.hasOwnProperty('university_id')) {
      cur_id = this.props.item.university_id
      card = <UniversityCard id={cur_id} item={this.props.item} key={cur_id}/>
    } else if (this.props.item.hasOwnProperty('degree_id')) {
      cur_id = this.props.item.degree_id
      card = <DegreeCard id={cur_id} item={this.props.item} key={cur_id} userLocation={this.state.userLocation}/>
    } else if (this.props.item.hasOwnProperty('city_id')) {
      cur_id = this.props.item.city_id
      card = <CityCard id={cur_id} item={this.props.item} key={cur_id}/>
    }

    return (<Grid key={cur_id}>
      {card}
    </Grid>);
  }
}

export default SearchResultsCardContainer;
