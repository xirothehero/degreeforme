import React, { Component } from 'react'
import PropTypes from "prop-types";
import Sort from './Sort.js';
import Typography from '@material-ui/core/Typography';
import MinMaxInput from '../Filter/MinMaxInput.js';
import noUiSlider from '../Filter/Slider.js';

function getChoices() {
    let choices = [
        { value: 'degree', displayName: 'Name', defOrder: 'asc'},
        { value: 'rank', displayName: 'Income ranking', defOrder: 'asc'},
        { value: 'meaningful_work', displayName: 'Job relation', defOrder: 'desc'},
        { value: 'pay_early', displayName: 'Entry level salary', defOrder: 'desc'},
        { value: 'pay_mid', displayName: 'Mid-level salary', defOrder: 'desc'}
      ];
    return choices;
};


const styles = {
  filterText: {
      'fontWeight': 600,
  },
  coloredText: {
      'fontWeight': 600,
      'color': '#E4C400' // accent color
  }
};

class DegreeFilterSortBar extends Component {
  render() {
    return (
      <div className="filter-sort-container">
        <div className="filter-body">
          <Typography variant='subtitle1' align='left' style={styles.coloredText}>Filter by</Typography>
          <div className="filter-item">
            <Typography variant='subtitle2' align='left' style={styles.filterText}>Entry level salary</Typography>
            <MinMaxInput query="salary" returnFilter={this.props.updateSalaryFilterCallback.bind(this)}/>
          </div>
          {/* <div className="filter-item">
            <Typography variant='subtitle2' align='left' style={styles.filterText}>Mid-level salary</Typography>
            <MinMaxInput query="salary" returnFilter={this.props.updateSalaryFilterCallback.bind(this)}/>
          </div> */}
        </div>
        <div className="sort-body">
          <Typography variant='subtitle1' align='left' style={styles.coloredText}>Sort by</Typography>
          <Sort options={getChoices()} updateSortData={this.props.updateDegreeSortData.bind(this)}/>
        </div>
      </div>
    )
  }
}

DegreeFilterSortBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default DegreeFilterSortBar;
