import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import {Link} from "react-router-dom";
import Highlighter from "react-highlight-words";
import {getHighlightWords, getSnippets} from '../../Assets/Helper.js';

const styles = {
  card: {
    maxWidth: '100%',
    display: 'flex',
    flexDirection: 'row'
  },
  media: {
    width: '35%',
    height: '100%',
    float: 'left',
    marginBottom: '5px',
    marginRight: '15px'
  },
  mediaColumn: {
    height: '100%',
    float: 'left'
  },
  chip: {
    margin: 2
  },
  content: {
    flex: '1 0 auto',
    height: '200px'
  },
  icon: {
    fontSize: '18px',
    verticalAlign: 'top',
    paddingBottom: '1px',
    paddingRight: '5px',
    position: 'relative',
    top: '-1px',
    color: '#1BC86C'
  }
};

class DegreeSearchResultCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      photo: ""
    }
  }
  componentDidMount() {
    if (this.props.item.photo_url !== "") {
      this.setState({photo: this.props.item.photo_url});
    }
  };
  render() {
    return (<Card className={this.props.classes.card} id={this.props.id}>
      <CardActionArea component={Link} to={"/degree/" + this.props.item.degree}>
        <CardMedia className={this.props.classes.media} image={this.state.photo} title=""></CardMedia>
        <CardContent className={this.props.classes.content} overflow='hidden'>
          <Typography id='DegreeCardName' gutterBottom="gutterBottom" variant="h5" component="h2" align="left">
            <Highlighter highlightClassName="YourHighlightClass" searchWords={getHighlightWords(this.props.query, this.props.item.degree)}
              // search terms to highlight
              autoEscape={true} textToHighlight={this.props.item.degree}/>
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>)
  }

}
DegreeSearchResultCard.propTypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(DegreeSearchResultCard);
