import React from 'react';
import { Link } from 'react-router-dom';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

const SplashCard = (props) => {
  return(
    <Card className="splashCard">
      <CardActionArea component={Link} to={{
            pathname:props.path,
          }}>
        <CardMedia
          className="splashCardMedia"
          image={props.photo}
          title={props.name}>
        </CardMedia>
        <CardContent style={{maxHeight: '125px', overflow:'hidden'}}>
          <Typography gutterBottom variant="h5" component="h2">
              {props.name}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  )
}

export default (SplashCard);
