import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab'

import logo from '../Assets/navbar_logo.svg'; // Tell Webpack this JS file uses this image

const styles = {
  textColorSecondary: {
    color: 'white'
  }
};

class Navbar extends Component {

  constructor(props) {
    super(props);

    var initialTabValue = null;
    let pathname = props.location.pathname

    if (pathname.startsWith("/university")) {
      initialTabValue = 0;
    } else if (pathname.startsWith("/university")) {
      initialTabValue = 1;
    } else if (pathname.startsWith("/degree")) {
      initialTabValue = 2;
    } else if (pathname.startsWith("/city")) {
      initialTabValue = 3;
    } else if (pathname.startsWith("/about")) {
      initialTabValue = 4;
    } else {
      initialTabValue = null;
    }

    this.state = {
      value: initialTabValue
    };
  }

  handleChange = (event, value) => {
    this.setState({value});
  };

  handleHomeClick = () => {
    this.setState({value: null})
  }

  render() {
    return (<div>
      <AppBar position='sticky' style={{
          background: '#1C1C1C',
          position: "fixed"
        }}>
        <Toolbar>
          <Grid justify="space-between" container="container" spacing={16} style={{
              alignItems: 'center'
            }}>
            <Grid id="homeLink" item="item" onClick={this.handleHomeClick} component={Link} to="/" >
              <img className="logo" src={logo} alt="Logo" />
            </Grid>
            <Grid item="item">
              <Tabs value={this.state.value} onChange={this.handleChange} textColor="secondary">
                {/* <Tab id='visualNavButton' component={Link} to="/visuals" label="Visuals" style={styles.textColorSecondary}/> */}
                <Tab id='universityNavButton' component={Link} to="/university" label="university" style={styles.textColorSecondary}/>
                <Tab id='degreeNavButton' component={Link} to="/degree" label="degree" style={styles.textColorSecondary}/>
                <Tab id='cityNavButton' component={Link} to="/city" label="city" style={styles.textColorSecondary}/>
                <Tab id='aboutNavButton' component={Link} to="/about" label="About" style={styles.textColorSecondary}/>
              </Tabs>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
    </div>)
  }
}

export default withRouter(Navbar);
