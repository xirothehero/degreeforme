import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class selenium_tests(unittest.TestCase) :

    def setUp(self) :
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
    
    def test1And2(self) :
        self.driver.get('https://www.degreeforme.me/')
        self.assertIn("Degree For Me", 
                    self.driver.title)
        degree_link = self.driver.find_element_by_xpath('//a[@href="/degree]')
        degree_link.click()
        a_tags = self.driver.find_element_by_tag_name("a")
        page_has_degree = False
        for tag in a_tags:
            tester = self.driver.find_element_by_link_text(tag.txt)
            if "degree" in tester.text :
                page_has_degree = True
                break
        assert page_has_degree
        assert "degree" in self.driver.current_url
    
    def test3(self) :
        university_link = self.driver.find_element_by_xpath('//a[@href="/university]')
        university_link.click()
        a_tags = self.driver.find_element_by_tag_name("a")
        page_has_university = False
        for tag in a_tags:
            tester = self.driver.find_element_by_link_text(tag.txt)
            if "university" in tester.text :
                page_has_university = True
                break
        assert page_has_university
        assert "university" in self.driver.current_url
    
    def test4(self) :
        city_link = self.driver.find_element_by_xpath('//a[@href="/city]')
        city_link.click()
        a_tags = self.driver.find_element_by_tag_name("a")
        page_has_city = False
        for tag in a_tags:
            tester = self.driver.find_element_by_link_text(tag.txt)
            if "city" in tester.text :
                page_has_city = True
                break
        assert page_has_city
        assert "city" in self.driver.current_url
        
    def tearDown(self) :
        self.driver.quit()
    
    if __name__ == "__main":
        main()
