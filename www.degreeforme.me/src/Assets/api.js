import axios from 'axios'

const apiBase = 'https://api.degreeforme.me/api'

class API {
  // filter universities https://api.degreeforme.me/api/colleges?q={"filters":[{"name":"name","op":"eq","val":"The University of Texas at Austin"}]}
  // if no params are provided, get all universities
  static async getUniversities(params) {
    return axios.get(apiBase + '/colleges', {params: params});
    // return axios.get(apiBase + '/colleges?q={"filters":[{"name":"id","op":"ge","val":" "}]}', {params: params});
  }
  

  static async searchUniversities(params) {
    console.log("in searchUniv, params:", params)
    var attributesToSearchKeyword = ["name", "city", "top_program"];
    var searchURL = createSearchURL("colleges", attributesToSearchKeyword, params);
    return axios.get(searchURL);
  }

  // get a specific university
  static async getUniversity(id) {
    return axios.get(apiBase + '/colleges/' + id)
  }

  static async getUniversityFromName(name) {
    return axios.get(apiBase + '/colleges?q={"filters":[{"name":"name","op":"eq","val":"' + name + '"}]}')
  }


  static async getUniversitiesForDegree(degree_name) {
    return axios.get(apiBase + '/colleges?q={"filters":[{"name":"top_program","op":"eq","val":"' + degree_name + '"}]}')
  }

  // TODO: not implemented but would be ideal
  // static async getUniversitiesForDegree(id) {
  //   return axios.get(apiBase + '/colleges/degree/' + id)
  // }

  static async getUniversitiesForCity(city_name) {
    return axios.get(apiBase + '/colleges?q={"filters":[{"name":"city","op":"eq","val":"' + city_name + '"}]}')
  }

  static async getDegrees(params) {
    return axios.get(apiBase + '/degrees?q={"filters":[{"name":"degree","op":"ge","val":" "}]}', {params: params});
  }

  // get a specific degree using its id TODO: UNUSED cause we haven't implemented that yet
  static async getDegreeFromId(id) {
    return axios.get(apiBase + '/degrees/' + id)
  }

  // get a specific degree using its name
  static async getDegree(name) {
    return axios.get(apiBase + '/degrees/' + name)
  }


  static async searchDegrees(params) {
    var attributesToSearchKeyword = ["degree", "job1", "job2"];
    var searchURL = createSearchURL("degrees", attributesToSearchKeyword, params);
    return axios.get(searchURL);
  }

  static async getCities(params) {
    return axios.get(apiBase + '/cities?q={"filters":[{"name":"id","op":"ge","val":" "}]}', {params: params});
  }

  // get a specific city using its id
  static async getCityFromId(id) {
    return axios.get(apiBase + '/cities/' + id);
  }

  // get a specific city using its name
  static async getCity(name) {
    return axios.get(apiBase + '/cities?q={"filters":[{"name":"name","op":"eq","val":"'+ name +'"}]}');
  }

  // TODO: not implemented but would be ideal
  // static async getCityForDegree(id) {
  //   return axios.get(apiBase + '/cities/degree/' + id)
  // }


  static async searchCities(params) {
    var attributesToSearchKeyword = ["name", "state"];
    var searchURL = createSearchURL("cities", attributesToSearchKeyword, params);
    return axios.get(searchURL);
  }

  static async getCityNames() {
    return axios.get(apiBase + '/cities/name');
  }

  static async getDegreeCategories() {
    return axios.get(apiBase + '/degrees/categories')
  }

}

export default API;


// return boolean if there are other parameters besides sort in the query
function areOtherFilters(params) {
  for(var key in params) {
    if(key != "sort" && key != "page" && key != "order") {
      return true;
    }
  }
  return false;
}

function createSearchURL(model, attributesToSearchKeyword, params) {
  var searchURL = apiBase + '/'+ model +'?q={';
  var sort = params.sort;
  var areOtherFiltersBesidesSort = areOtherFilters(params); //if there are other parameters besides sort in the query
  console.log("create search url, params:", params)
  if(attributesToSearchKeyword.length > 0) {
    var keyword = params.keyword;
    if(areOtherFiltersBesidesSort) {
      searchURL +='"filters":[';
      if(keyword) {
        // add the first attribute to search by keyword
        var a = attributesToSearchKeyword[0];
        searchURL += '{"or":[{"name":"'+ a +'","op":"like","val":"%'+ keyword +'%"}';

        // add the rest of the keyword search attributes with a comma in front
        for(var i = 1; i < attributesToSearchKeyword.length; i++) {
          var a = attributesToSearchKeyword[i];
          searchURL += ',{"name":"'+ a +'","op":"like","val":"%'+ keyword +'%"}';
        }
        searchURL += ']}';
      }
    }
    switch(model) {
      case "colleges":
        var sat_score = params.sat_score;
        if(areOtherFiltersBesidesSort) {
          if(sat_score) {
            if(keyword) {
              searchURL += ',';
            }
            searchURL += '{"name":"sat_scores","op":"le","val":"'+ sat_score +'"}';
          }
          if(sort) {
            searchURL += ',{"name":"'+ sort +'","op":"gt","val":""}';
          }
        }
        break;

      case "degrees":
          console.log("in api.js DEGREE case, params:", params)
          var entryMax = params.entryMax ? params.entryMax : 9999999;
          var entryMin = params.entryMin ? params.entryMin : 0;

          if(areOtherFiltersBesidesSort) {
            if(keyword) {
              searchURL += ',';
            }
            searchURL += '{"name":"pay_early","op":"le","val":'+ entryMax +'},{"name":"pay_early","op":"gt","val":'+ entryMin +'}';
          }
        break;

      case "cities":
        if(!keyword && !params.costMax && !params.costMin && !sort) {
          // get all cities
          searchURL += '"filters":[{"name":"id","op":"ge","val":" "}]';
        } 
        else {
          var costMax = params.costMax ? params.costMax : 9999999;
          var costMin = params.costMin ? params.costMin : 0;

          if(areOtherFiltersBesidesSort) {
            if(keyword) {
              searchURL += ',';
            }
            searchURL += '{"name":"housing","op":"le","val":'+ costMax +'},{"name":"housing","op":"gt","val":'+ costMin +'}';
          }
        }
        break;
      default:
        console.error("Must provide a valid model to search API.");
        break;
      }
  }
  else {
    console.error("Must search through at least 1 attribute.");
  }
  
  
  if(areOtherFiltersBesidesSort) {
    searchURL += ']';
  }
  if(sort) {
    if(areOtherFiltersBesidesSort) {
      searchURL += ',';
    }
    searchURL += '"order_by":[{"field":"'+ sort +'","direction":"'+ params.order +'"}]';
    if(!areOtherFiltersBesidesSort) {
      searchURL += ',"filters":[{"name":"'+sort+'","op":"gt","val":""}]';
    }
  }
  searchURL += '}&page=' + params.page;
  console.log("searchURL:",searchURL)
  return searchURL;
}

