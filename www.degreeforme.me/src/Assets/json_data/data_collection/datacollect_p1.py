#!/usr/bin/env python3

import json
import pickle
import re
import requests
from sys import stdout
from typing import IO
import time
import urllib.request
from bs4 import BeautifulSoup
# import pandas as pd

def get_college_data(writer: IO[str]):
    url = "https://api.data.gov/ed/collegescorecard/v1/schools.json?"
    search = "school.name=university%20of%20texas"
    field_keyword = "&_fields="
    fields = "school.name,id,location.lat,location.lon,school.city," \
            + "school.ownership_peps," \
            + "latest.admissions.sat_scores.average.overall," \
            + "latest.admissions.admission_rate.overall," \
            + "latest.cost.tuition.in_state,latest.cost.tuition.out_of_state," \
            + "latest.completion.completion_rate_4yr_150nt," \
            + "latest.academics.program_percentage.agriculture," \
            + "latest.academics.program_percentage.resources," \
            + "latest.academics.program_percentage.architecture," \
            + "latest.academics.program_percentage.ethnic_cultural_gender," \
            + "latest.academics.program_percentage.communication," \
            + "latest.academics.program_percentage.communications_technology," \
            + "latest.academics.program_percentage.computer," \
            + "latest.academics.program_percentage.personal_culinary," \
            + "latest.academics.program_percentage.education," \
            + "latest.academics.program_percentage.engineering," \
            + "latest.academics.program_percentage.engineering_technology," \
            + "latest.academics.program_percentage.language," \
            + "latest.academics.program_percentage.family_consumer_science," \
            + "latest.academics.program_percentage.legal," \
            + "latest.academics.program_percentage.english," \
            + "latest.academics.program_percentage.humanities," \
            + "latest.academics.program_percentage.library," \
            + "latest.academics.program_percentage.biological," \
            + "latest.academics.program_percentage.mathematics," \
            + "latest.academics.program_percentage.military," \
            + "latest.academics.program_percentage.multidiscipline," \
            + "latest.academics.program_percentage.parks_recreation_fitness," \
            + "latest.academics.program_percentage.philosophy_religious," \
            + "latest.academics.program_percentage.theology_religious_vocation," \
            + "latest.academics.program_percentage.physical_science," \
            + "latest.academics.program_percentage.science_technology," \
            + "latest.academics.program_percentage.psychology," \
            + "latest.academics.program_percentage.security_law_enforcement," \
            + "latest.academics.program_percentage.public_administration_social_service," \
            + "latest.academics.program_percentage.social_science," \
            + "latest.academics.program_percentage.construction," \
            + "latest.academics.program_percentage.mechanic_repair_technology," \
            + "latest.academics.program_percentage.precision_production," \
            + "latest.academics.program_percentage.transportation," \
            + "latest.academics.program_percentage.visual_performing," \
            + "latest.academics.program_percentage.health," \
            + "latest.academics.program_percentage.business_marketing," \
            + "latest.academics.program_percentage.history," \
            + "latest.student.demographics.race_ethnicity.white," \
            + "latest.student.demographics.race_ethnicity.black," \
            + "latest.student.demographics.race_ethnicity.hispanic," \
            + "latest.student.demographics.race_ethnicity.asian," \
            + "latest.student.demographics.race_ethnicity.aian," \
            + "latest.student.demographics.race_ethnicity.nhpi," \
            + "latest.student.demographics.race_ethnicity.two_or_more," \
            + "latest.student.demographics.race_ethnicity.non_resident_alien," \
            + "latest.student.demographics.race_ethnicity.unknown," \
            + "latest.student.demographics.race_ethnicity.white_non_hispanic," \
            + "latest.student.demographics.race_ethnicity.black_non_hispanic," \
            + "latest.student.demographics.race_ethnicity.asian_pacific_islander"
    sort_keyword = "&sort="
    sort = "school.name:asc"
    api_key = "&api_key=jE2DrEDMtC41ajO3C78aZAOk6CDKmlHhbYcSq7ca"
    per_page = "&per_page=100"
    page = "&page=1"

    full_url = url + field_keyword + fields + sort_keyword + sort + api_key + per_page
    pickle_list = []
    for j in range(0,71):
        response = requests.get(full_url + "&page=" + str(j))
        response.json()
        data = json.loads(response.text)
        program_percentage_keys = []
        for k in list(data['results'][0].keys()):
            if 'latest.academics.program_percentage' in k:
                program_percentage_keys.append(k)
        rng = 100
        if j == 70:
            rng = 58
        for i in range(rng):

            program_percentage = { key : data['results'][i][key] for key in program_percentage_keys }
        
            top_program = program_percentage_keys[0]
            for k in program_percentage_keys:
                if program_percentage[k] is None:
                    pass
                elif program_percentage[k] > program_percentage[top_program]:
                    top_program = k

            if program_percentage[top_program] is None:
                top_program = "N/A"
            top_program = re.sub('latest.academics.program_percentage.','', top_program)
            data['results'][i]['top_program'] = top_program

            pickle_list.append(data['results'][i])

        # writer.write(str(json.dumps(data, indent=4)) + "\n")

    pickle.dump(pickle_list, open("college_data.p", "wb"))

def get_college_cities(writer: IO[str]):
    url = "https://api.data.gov/ed/collegescorecard/v1/schools.json?"
    search = "school.name=university%20of%20texas"
    field_keyword = "&_fields="
    fields = "school.name,id,location.lat,location.lon,school.city," \
            + "school.ownership_peps," \
            + "latest.admissions.sat_scores.average.overall," \
            + "latest.admissions.admission_rate.overall," \
            + "latest.cost.tuition.in_state,latest.cost.tuition.out_of_state," \
            + "latest.completion.completion_rate_4yr_150nt," \
            + "latest.academics.program_percentage.agriculture," \
            + "latest.academics.program_percentage.resources," \
            + "latest.academics.program_percentage.architecture," \
            + "latest.academics.program_percentage.ethnic_cultural_gender," \
            + "latest.academics.program_percentage.communication," \
            + "latest.academics.program_percentage.communications_technology," \
            + "latest.academics.program_percentage.computer," \
            + "latest.academics.program_percentage.personal_culinary," \
            + "latest.academics.program_percentage.education," \
            + "latest.academics.program_percentage.engineering," \
            + "latest.academics.program_percentage.engineering_technology," \
            + "latest.academics.program_percentage.language," \
            + "latest.academics.program_percentage.family_consumer_science," \
            + "latest.academics.program_percentage.legal," \
            + "latest.academics.program_percentage.english," \
            + "latest.academics.program_percentage.humanities," \
            + "latest.academics.program_percentage.library," \
            + "latest.academics.program_percentage.biological," \
            + "latest.academics.program_percentage.mathematics," \
            + "latest.academics.program_percentage.military," \
            + "latest.academics.program_percentage.multidiscipline," \
            + "latest.academics.program_percentage.parks_recreation_fitness," \
            + "latest.academics.program_percentage.philosophy_religious," \
            + "latest.academics.program_percentage.theology_religious_vocation," \
            + "latest.academics.program_percentage.physical_science," \
            + "latest.academics.program_percentage.science_technology," \
            + "latest.academics.program_percentage.psychology," \
            + "latest.academics.program_percentage.security_law_enforcement," \
            + "latest.academics.program_percentage.public_administration_social_service," \
            + "latest.academics.program_percentage.social_science," \
            + "latest.academics.program_percentage.construction," \
            + "latest.academics.program_percentage.mechanic_repair_technology," \
            + "latest.academics.program_percentage.precision_production," \
            + "latest.academics.program_percentage.transportation," \
            + "latest.academics.program_percentage.visual_performing," \
            + "latest.academics.program_percentage.health," \
            + "latest.academics.program_percentage.business_marketing," \
            + "latest.academics.program_percentage.history," \
            + "latest.student.demographics.race_ethnicity.white," \
            + "latest.student.demographics.race_ethnicity.black," \
            + "latest.student.demographics.race_ethnicity.hispanic," \
            + "latest.student.demographics.race_ethnicity.asian," \
            + "latest.student.demographics.race_ethnicity.aian," \
            + "latest.student.demographics.race_ethnicity.nhpi," \
            + "latest.student.demographics.race_ethnicity.two_or_more," \
            + "latest.student.demographics.race_ethnicity.non_resident_alien," \
            + "latest.student.demographics.race_ethnicity.unknown," \
            + "latest.student.demographics.race_ethnicity.white_non_hispanic," \
            + "latest.student.demographics.race_ethnicity.black_non_hispanic," \
            + "latest.student.demographics.race_ethnicity.asian_pacific_islander"
    sort_keyword = "&sort="
    sort = "school.name:asc"
    api_key = "&api_key=jE2DrEDMtC41ajO3C78aZAOk6CDKmlHhbYcSq7ca"
    per_page = "&per_page=100"
    page = "&page=1"

    cities = set()
    full_url = url + field_keyword + fields + sort_keyword + sort + api_key + per_page
    for j in range(0,71):
        response = requests.get(full_url + "&page=" + str(j))
        response.json()
        data = json.loads(response.text)
        program_percentage_keys = []
        for k in list(data['results'][0].keys()):
            if 'latest.academics.program_percentage' in k:
                program_percentage_keys.append(k)
        rng = 100
        if j == 70:
            rng = 58
        for i in range(rng):
            city_name = data['results'][i]['school.city']
            if "North " in city_name or "South " in city_name or "East " in city_name or "West " in city_name:
                pass
            else:
                cities.add(city_name)
    
    # writer.write(str(cities))
    print(len(cities))

def get_city_data(writer: IO[str]):
    cities = []
    
    url = "http://www.city-data.com/city/Austin-Texas.html"
    response = requests.get(url)
    soup = BeautifulSoup(response.text, "html.parser")
    austin = ["Austin"]
    pop = soup.find('section',{'class':'city-population'})
    pop = re.sub('<[^<]+?>', '', str(pop))
    austin.append(pop)
    med_age = soup.find('section',{'class':'median-age'}).findAll('td')[1]
    med_age = re.sub('<[^<]+?>', '', str(med_age))
    med_age = re.sub('\xa0', '', med_age)
    austin.append(med_age)
    med_inc = soup.find('section',{'class':'median-income'}).findAll('td')[1]
    med_inc = re.sub('<[^<]+?>', '', str(med_inc))
    austin.append(med_inc)
    med_rent = soup.find('section',{'class':'median-rent'})
    med_rent = re.sub('<[^<]+?>', '', str(med_rent))
    austin.append(med_rent)
    cities.append(austin)

    url = "http://www.city-data.com/city/Dallas-Texas.html"
    response = requests.get(url)
    soup = BeautifulSoup(response.text, "html.parser")
    dallas = ["Dallas"]
    pop = soup.find('section',{'class':'city-population'})
    pop = re.sub('<[^<]+?>', '', str(pop))
    dallas.append(pop)
    med_age = soup.find('section',{'class':'median-age'}).findAll('td')[1]
    med_age = re.sub('<[^<]+?>', '', str(med_age))
    med_age = re.sub('\xa0', '', med_age)
    dallas.append(med_age)
    med_inc = soup.find('section',{'class':'median-income'}).findAll('td')[1]
    med_inc = re.sub('<[^<]+?>', '', str(med_inc))
    dallas.append(med_inc)
    med_rent = soup.find('section',{'class':'median-rent'})
    med_rent = re.sub('<[^<]+?>', '', str(med_rent))
    dallas.append(med_rent)
    cities.append(dallas)

    url = "http://www.city-data.com/city/Denton-Texas.html"
    response = requests.get(url)
    soup = BeautifulSoup(response.text, "html.parser")
    denton = ["Denton"]
    pop = soup.find('section',{'class':'city-population'})
    pop = re.sub('<[^<]+?>', '', str(pop))
    denton.append(pop)
    med_age = soup.find('section',{'class':'median-age'}).findAll('td')[1]
    med_age = re.sub('<[^<]+?>', '', str(med_age))
    med_age = re.sub('\xa0', '', med_age)
    denton.append(med_age)
    med_inc = soup.find('section',{'class':'median-income'}).findAll('td')[1]
    med_inc = re.sub('<[^<]+?>', '', str(med_inc))
    denton.append(med_inc)
    med_rent = soup.find('section',{'class':'median-rent'})
    med_rent = re.sub('<[^<]+?>', '', str(med_rent))
    denton.append(med_rent)
    cities.append(denton)

    print("City Data\n\n")
    for city in cities:
        print(str(city) + "\n")
    

def get_degree_data(writer: IO[str]):
    url = 'https://www.timeshighereducation.com/student/news/college-degrees-highest-salary-potential#survey-answer'
    response = requests.get(url)
    soup = BeautifulSoup(response.text, "html.parser")
    table = soup.find('table')
    rows = table.findAll('tr')
    rows2 = []
    for tr in rows[1::]:
        entries = tr.findAll('td')
        row = []
        for td in entries:
            data = re.sub('<[^<]+?>', '', str(td.find('p')))
            data = re.sub('\xa0', '', data)
            data = re.sub('=', '', data)
            row.append(data)
        row_dict = {
            'rank' : int(row[0]),
            'degree' : row[1],
            'pay_early' : int(row[3].replace('$','').replace(',','')),
            'pay_mid' :  int(row[4].replace('$','').replace(',',''))
        }
        if row[5] == 'N/A':
            row_dict['meaningful_work'] = 0
        else:
            row_dict['meaningful_work'] = int(row[5].replace('%',''))
        rows2.append(row_dict)
    # print("Degree Data\n\n")
    # for line in rows2:
    #     print(str(line) + "\n")
    # print("\n")
    # print(str(json.dumps(rows2, indent=4)) + "\n")

    pickle.dump(rows2, open("degree_data.p", "wb"))

def pickle_check():
    college_dict = pickle.load(open("degree_data.p", "rb"))
    print(college_dict)

def json_to_pickle():
    jsonfile = open("college_data_2.json", "r")
    data = json.loads(jsonfile.read())
    pickle.dump(data['results'], open("college_data.p", "wb"))

if __name__ == "__main__":
    # get_college_data(stdout)
    # get_degree_data(stdout)
    # get_city_data(stdout)
    # get_college_cities(stdout)
    pickle_check()
    # json_to_pickle()