import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import './App.css';

import Splash from './Pages/Splash.js';
import University from './Pages/University.js';
import Degree from './Pages/Degree.js';
import City from './Pages/City.js';
import About from './Pages/About.js';
import DegreeInstance from './Pages/DegreeInstance.js';
import UniversityInstance from './Pages/UniversityInstance.js';
import CityInstance from './Pages/CityInstance.js';
import GlobalSearchResults from './Pages/GlobalSearchResults.js';

import 'typeface-roboto';
import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';

import NavBar from './Components/NavBar.js';
import Footer from './Components/Footer.js';

class App extends Component {
  render() {
    return (<MuiThemeProvider theme={theme}>
      <div className="App">
        <header>
          <NavBar/>
          <br/><br/><br/>
        </header>
        <Route path="/" exact component={Splash}/>
        <Route path="/university" exact component={University}/>
        <Route path="/city" exact component={City}/>
        <Route path="/degree" exact component={Degree}/>
        <Route path="/about" component={About}/>
        <Route path="/degreeInstance" component={DegreeInstance}/>
        <Route path="/university/:id" component={UniversityInstance}/>
        <Route path="/city/:id" component={CityInstance}/>
        <Route path="/degree/:id" component={DegreeInstance}/>
        <Route path="/search" component={GlobalSearchResults}/>
      </div> 
      <Footer/>
      
    </MuiThemeProvider>);
  }
}

let dark_color = '#1C1C1C';
let caption_color = '#494B4E';
let accent_color = '#E4C400';

  // global style
  const theme = createMuiTheme({
    typography: {
      fontFamily: 'Roboto',
      h2: {
        color: 'white',
        fontWeight: 'bold',
        paddingTop: 48,
        paddingBottom: 48
      },
      h4: {
        color: dark_color,
        fontWeight: 'bold',
        paddingBottom: 24
      },
      h5: {
        color: caption_color,
        fontWeight: 'bold'
      },
      h6: {
        color: caption_color,
        fontWeight: '600',
        paddingBottom: 15,
        textAlign: 'left'
      }
    },
    palette: {
      primary: {
        main: accent_color,
        light: accent_color,
        dark: accent_color
      },
      secondary: {
        main: accent_color
      }
    },
    overrides: {
      MuiPickersToolbar: {
        toolbar: {
          backgroundColor: accent_color
        }
      },
      MuiPickersDay: {
        day: {
          color: caption_color
        },
        isSelected: {
          backgroundColor: accent_color
        },
        current: {
          color: accent_color
        }
      },
      MuiPickersModal: {
        dialogAction: {
          color: caption_color
        }
      }
    }
  });

export default App;
