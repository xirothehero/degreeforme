import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import Enzyme from 'enzyme';
import { mount, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });
import App from './App';
import Splash from './Pages/Splash'
import University from './Pages/University'
import Degree from './Pages/Degree'
import City from './Pages/City'


describe('App runs', () => {
	it('App renders without crashing', () => {
       <Router>
            const div = document.createElement('div');
  			ReactDOM.render(<App />, div);
  			ReactDOM.unmountComponentAtNode(div);
       </Router>
	})
})

describe('Layout', function() {
    describe('Nav Bar', () => {
      it('App contains navigation bar', () => {
          const div = shallow(<App />)
          expect(div.find('Navigation'))
      })
    })
    describe('Splash', () => {
      it('App contains Splash', () => {
          const div = shallow(<App />)
          expect(div.find('Splash'))
      })
    })
    describe('Model Pages', () => {
      it('App contains Models', () => {
          const div = shallow(<App />)
          expect(div.find('University'))
          expect(div.find('City'))
          expect(div.find('Degree'))
      })
    })
    describe('Instance Pages', () => {
      it('App contains Instances', () => {
          const div = shallow(<App />)
          expect(div.find('UniversityInstance'))
          expect(div.find('CityDetail'))
          expect(div.find('DegreeInstance'))
      })
    })
    describe('About', () => {
      it('App contains About', () => {
          const div = shallow(<App />)
          expect(div.find('About'))
      })
    })
})

describe('Instances', function() {
    describe('Degree to Univerity', () => {
      it('Degree contains link to Universities', () => {
          const div = shallow(<Degree />)
          expect(div.find('Popular universities')) 
      })
    })
    describe('Degree to City', () => {
      it('Degree contains link to Cities', () => {
          const div = shallow(<Degree />)
          expect(div.find('Cities')) 
      })
    })
    describe('City to University', () => {
      it('City contains link to Universities', () => {
          const div = shallow(<City />)
          expect(div.find('Popular universities')) 
      })
    })
    describe('City to Degree', () => {
      it('City contains link to Degrees', () => {
          const div = shallow(<City />)
          expect(div.find('Popular degrees')) 
      })
    })

    describe('University to City', () => {
      it('University contains link to City', () => {
          const div = shallow(<University />)
          expect(div.find('City')) 
      })
    })
    describe('University to Degree', () => {
      it('University contains link to Degrees', () => {
          const div = shallow(<University />)
          expect(div.find('Degrees')) 
      })
    })
})

describe('Components', function() {
    describe('Splash Card', () => {
      it('Splash page renders splash cards', () => {
          const div = shallow(<Splash />)
          expect(div.find('SplashCard'))
      })
    })
    describe('University Card', () => {
      it('University page renders university cards', () => {
          const div = shallow(<University />)
          expect(div.find('UniversityCard'))
      })
    })
    describe('Degree Card', () => {
      it('Degree page renders degree cards', () => {
          const div = shallow(<Degree />)
          expect(div.find('DegreeCard'))
      })
    })
    describe('City Card', () => {
      it('City page renders city cards', () => {
          const div = shallow(<City />)
          expect(div.find('CityCard'))
      })
    })
})