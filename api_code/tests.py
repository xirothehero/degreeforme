from unittest import TestCase, main
import os
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask import Flask, jsonify, request
import json

import { get_popoular_data_for_city } from scraping_and_cleaning
import { get_col_info } from city_info_scraping

class TestApplication(TestCase):

    def city_top_program_test(self):
        cp = get_popoular_data_for_city("Austin")
        assert cp == ("The University of Texas at Austin", "Business and marketing")

    def city_scrape_test(self):
        c, s = get_col_info("Austin", "Texas")
        assert type(c) is dict
        assert c['state'] == "Texas"

