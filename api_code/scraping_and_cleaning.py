from google_images_download import google_images_download  
import pickle
import sys
import functools 

# creating object 
response = google_images_download.googleimagesdownload()    
# Code for this functino obtained from https://www.geeksforgeeks.org/how-to-download-google-images-using-python/
def downloadimages(query): 
    # keywords is the search query 
    # format is the image file format 
    # limit is the number of images to be downloaded 
    # print urs is to print the image file url 
    # size is the image size which can 
    # be specified manually ("large, medium, icon") 
    # aspect ratio denotes the height width ratio 
    # of images to download. ("tall, square, wide, panoramic") 
    arguments = {"keywords": query, "format": "jpg", "limit":1, "print_urls":True, "size": "large"} 
    try:
        orig_stdout = sys.stdout
        f = open('URLS.txt', 'w')
        sys.stdout = f
        paths = response.download(arguments)

        sys.stdout = orig_stdout
        f.close()

        with open('URLS.txt') as f:
            content = f.readlines()
        f.close()

        urls = []
        for j in range(len(content)):
            if content[j][:9] == 'Completed':
                urls.append(content[j-1][11:-1])   
        return urls[0]
    # Handling File NotFound Error     
    except FileNotFoundError:
        pass

def get_col_imgs():
    city_data = pickle.load(open("master_city_list.p", "rb"))
    city_list = [c['name'].lower() for c in city_data]

    result = []
    college_data = pickle.load(open("college_data.p", "rb"))
    i = 0
    for college in college_data:
        i += 1
        print (i)
        if (college['city'].lower() in city_list):
            url = downloadimages(college['name'] + ' college')
            college['photo_url'] = url
            result.append(college)
            print (college)

    pickle.dump(result, open("refined_college_data.p", "wb"))

def deg_degree_imgs():
    degree_map = {'architecture': 'Architecture', 'transportation': 'Transportation and logistics management', 'visual_performing': 'Visual arts (VA)', "psychology": "Psychology", 'theology_religious_vocation': "Theology", 'computer': 'Computer science (CS) and engineering', 'physical_science': 'Physical therapy', 'communication': 'Communication','social_science': 'Social science','engineering': 'Petroleum engineering', 'business_marketing': 'Business and marketing', 'philosophy_religious': 'Philosophy', 'mathematics': 'Mathematics', 'language': 'Foreign languages', 'military': "None", 'family_consumer_science': 'Family and consumer science', 'engineering_technology': 'Systems engineering', 'science_technology': 'Medical laboratory technology', 'legal': 'Legal studies', 'humanities': 'Humanities', 'public_administration_social_service': 'Public administration', 'multidiscipline': 'None', 'agriculture': 'Agriculture', 'parks_recreation_fitness': 'Parks and recreation management', 'education': 'Education', 'history': 'History', 'personal_culinary': 'Culinary arts', 'health': 'Health', 'mechanic_repair_technology': 'None', 'biological': 'Biological sciences', 'communications_technology': 'Technical communication', 'security_law_enforcement': "None", 'construction': 'Building construction management', 'english': 'English Literature'}
    degrees = degree_map.keys()
    results = []
    for d in degrees:
        print (d)
        url = downloadimages(d.split("_"))
        new = {degree_map[d]:url}
        print (new)
        results.append(new)

"""
degree_map = {'architecture': 'Architecture', 'transportation': 'Transportation and logistics management', 'visual_performing': 'Visual arts (VA)', "psychology": "Psychology", 'theology_religious_vocation': "Theology", 'computer': 'Computer science (CS) and engineering', 'physical_science': 'Physical therapy', 'communication': 'Communication','social_science': 'Social science','engineering': 'Petroleum engineering', 'business_marketing': 'Business and marketing', 'philosophy_religious': 'Philosophy', 'mathematics': 'Mathematics', 'language': 'Foreign languages', 'military': "None", 'family_consumer_science': 'Family and consumer science', 'engineering_technology': 'Systems engineering', 'science_technology': 'Medical laboratory technology', 'legal': 'Legal studies', 'humanities': 'Humanities', 'public_administration_social_service': 'Public administration', 'multidiscipline': 'None', 'agriculture': 'Agriculture', 'parks_recreation_fitness': 'Parks and recreation management', 'education': 'Education', 'history': 'History', 'personal_culinary': 'Culinary arts', 'health': 'Health', 'mechanic_repair_technology': 'None', 'biological': 'Biological sciences', 'communications_technology': 'Technical communication', 'security_law_enforcement': "None", 'construction': 'Building construction management', 'english': 'English Literature'}
college_data = pickle.load(open("refined_college_data3.p", "rb"))
result = 0
for c in college_data:
    result = max(result, len(c['photo_url']))
print (result)
"""

def get_most_popular_data_for_degree(degree):
    college_data = pickle.load(open("refined_college_data3.p", "rb"))
    m = 0
    name = ""
    city = ""
    degree_map = {'architecture': 'Architecture', 'transportation': 'Transportation and logistics management', 'visual_performing': 'Visual arts (VA)', "psychology": "Psychology", 'theology_religious_vocation': "Theology", 'computer': 'Computer science (CS) and engineering', 'physical_science': 'Physical therapy', 'communication': 'Communication','social_science': 'Social science','engineering': 'Petroleum engineering', 'business_marketing': 'Business and marketing', 'philosophy_religious': 'Philosophy', 'mathematics': 'Mathematics', 'language': 'Foreign languages', 'military': "None", 'family_consumer_science': 'Family and consumer science', 'engineering_technology': 'Systems engineering', 'science_technology': 'Medical laboratory technology', 'legal': 'Legal studies', 'humanities': 'Humanities', 'public_administration_social_service': 'Public administration', 'multidiscipline': 'None', 'agriculture': 'Agriculture', 'parks_recreation_fitness': 'Parks and recreation management', 'education': 'Education', 'history': 'History', 'personal_culinary': 'Culinary arts', 'health': 'Health', 'mechanic_repair_technology': 'None', 'biological': 'Biological sciences', 'communications_technology': 'Technical communication', 'security_law_enforcement': "None", 'construction': 'Building construction management', 'english': 'English Literature'}
    inverted_dict = dict([[v,k] for k,v in degree_map.items()])
    if (degree not in inverted_dict.keys()):
        return None
    for c in college_data:
        if (degree in inverted_dict.keys() and inverted_dict[degree] in c.keys() and c[inverted_dict[degree]] > m):
            m = c[inverted_dict[degree]]
            name = c['name']
            city = c['city']
    return (name, city)

def clean_degree_data():
    degree_data = pickle.load(open("degree_data.p", "rb"))
    result = []
    for d in degree_data:
        ret = get_most_popular_data_for_degree(d['degree'])
        if (ret != None):
            d['college1'] = ret[0]
            d['city1'] = ret[1]
            result.append(d)
    pickle.dump(result, open("refined_degree_data.p", "wb"))

def clean_city_data():
    data = pickle.load(open("city_data.p", "rb"))
    college_data = pickle.load(open("refined_college_data3.p", "rb"))
    result = []
    for d in data:
        hasOne = False
        for c in college_data:
            if (d['name'].lower() == c['city'].lower()):
                hasOne = True
                break
        if (hasOne):
            result.append(d)
    pickle.dump(result, open("refined_city_data.p", "wb"))


def get_city_imgs():
    data = pickle.load(open("refined_city_data.p", "rb"))
    result = []
    x = 0
    m = 0
    for d in data:
        url = downloadimages(d['name'] + " city, " +d['state'])
        x += 1
        print (x)
        m = max(len(url), m)
        d['photo_url'] = url
        result.append(d)
    pickle.dump(result, open("refined_city_data2.p", "wb"))
    print ("LENGTH: " + str(m))


def get_popoular_data_for_city(city):
    college_data = pickle.load(open("refined_college_data3.p", "rb"))
    name = None
    degree = None
    for c in college_data:
        if (city.lower() == c['city'].lower()):
            name = c['name']
            degree = c['top_program']
            break
    return (name, degree)

def refine_city_data2():
    data = pickle.load(open("refined_city_data2.p", "rb"))
    result = []
    c_len = 0
    d_len = 0
    for city in data:
        res = get_popoular_data_for_city(city['name'])
        if (res[0] != None and res[1] != None):
            city['college'] = res[0]
            city['degree'] = res[1]
            result.append(city)
            print (city)
            c_len = max(c_len, len(res[0]))
            d_len = max(d_len, len(res[1]))
    pickle.dump(result, open("refined_city_data3.p", "wb"))
    print (c_len)
    print (d_len)


def refine_degree_data2():
    degree_urls = {'architecture':"""https://1sd06y38jhbh1xhqve6fqmc1-wpengine.netdna-ssl.com/wp-content/uploads/2017/02/fallingwater-1440x640.jpg""", 'Transportation and logistics management': 'https://upload.wikimedia.org/wikipedia/commons/d/d0/Air_Force_One_and_Limo.jpg', 'Visual Arts (va)': 'https://i0.wp.com/www.bayarea.com/wp-content/uploads/2017/07/GrandLake.jpg', 'Psychology': 'https://www.kvc.org/wp-content/uploads/2016/05/mental-health.png', 'Theology': 'https://factsandtrends.net/wp-content/uploads/2016/10/theology-680x419.jpg', 'computer science (cs) and engineering': 'https://cdn-images-1.medium.com/max/1600/1*x8vRMGXi-f-oNQ_0z03rLQ.jpeg', 'Physical Therapy': 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTVdJO13Ku69HQoXNFWXenC0RrP0uyqJO_5vFXl32Zk8nH43P1_hA', 'Communication': 'https://irishtechnews.ie/wp-content/uploads/2017/07/Communication-759x500.png?ezimgfmt=rs:750x494/rscb1/ng:webp/ngcb1', 'Social science': 'https://qswownews.com/wp-content/uploads/2018/05/SocialScience.jpg', 'Petroleum Engineering': 'http://tnstatenewsroom.com/wp-content/uploads/2019/03/engineering1.jpg', 'Business and marketing': 'https://curatti.com/wp-content/uploads/2018/02/Successful-Business-Marketing-Planning-1000x500.jpg', 'Philosophy': 'https://www.thoughtco.com/thmb/rvG-Ag0dx1b09Fhj7KrH01qCgQY=/1200x800/filters:fill(auto,1)/tax2_image_philosophy-58a22d1668a0972917bfb559.png', 'Mathematics': 'https://www.lockheedmartin.com/content/dam/lockheed-martin/eo/photo/webt/webt-woman-in-math-lead.jpg.pc-adaptive.full.medium.jpeg', 'Foreign languages': 'https://www.hpr2.org/sites/khpr/files/styles/medium/public/201901/33692_0.jpg', 'Family and consumer science': 'https://www.cbsd.org/cms/lib/PA01916442/Centricity/Domain/3248/FCS%20Logo.jpg', 'Systems Engineering': 'https://www.montanaitpros.com/wp-content/uploads/2012/10/home-slide-full-Morrison-Maierle-Systems-Technology-Consulting.jpg', 'Technical communication': 'https://trimech.com/uploads/products/_articleGalleryImage/Software-Page-Category-Image-Technical-Communcation-V3.png', 'Legal Studies': 'https://d32dsh9a6dg9hg.cloudfront.net/wp-content/uploads/sites/2/2018/09/06101540/Legal.jpg', 'Humanities': 'https://www.nwo.nl/binaries/articleImage/content/gallery/nwo/sgw/synergy-conference-4x3.jpg', 'Public administration': 'https://www.tuw.edu/wp-content/uploads/5-growing-careers-public-administration-870x350.jpg', 'Agriculture': 'https://www.plasticseurope.org/application/files/6615/1274/2893/5.6._aaheader.png', 'Parks and recreation management': 'https://icma.org/sites/default/files/parks-and-rec_1000x666.jpg', 'Education': 'http://www.vivateachers.org/wp-content/uploads/2018/08/student-education-750x460.jpg', 'History': 'https://jessicamdewitt.files.wordpress.com/2018/04/history-world-map.jpg', 'Culinary Arts': 'https://tricountytech.edu/wp-content/uploads/2018/04/CUL-Banner-1336x400.png', 'Health': 'https://www.mobihealthnews.com/sites/default/files/world%20health.jpg', 'Biological Sciences': 'https://dornsife.usc.edu/assets/img/news/story/2618.jpg', 'Medical laboratory technology': 'https://www.actx.edu/images/filecabinet/folder2/Medical_Lab.jpg', 'Building Construction Management': 'https://4b15vr13762d1t2kqp47j8d4-wpengine.netdna-ssl.com/wp-content/uploads/Depositphotos_119013316_m-2015.jpg'}
    degree_data = pickle.load(open("refined_degree_data2.p", "rb"))

    degree_urls =  {k.lower(): v for k, v in degree_urls.items()}

    m = functools.reduce(lambda a, b: max(a, len(b['photo_url'])), degree_data, 0)
    print (m)
    for d in degree_data:
        d['photo_url'] = degree_urls[d['degree'].lower()]

    #pickle.dump(degree_data, open("refined_degree_data2.p", "wb"))


refine_degree_data2()