"""
def upload_degree_data():
    p = open("../degree_data.p", "rb")
    p2 = open("../college_data.p", "rb")
    degrees = pickle.load(p)
    colleges = pickle.load(p2)
    degree_map = {'architecture': 'Architecture', 'transportation': 'Transportation and logistics management', 'visual_performing': 'Visual arts (VA)', "psychology": "Psychology", 'theology_religious_vocation': "Theology", 'computer': 'Computer science (CS) and engineering', 'physical_science': 'Physical therapy', 'communication': 'Communication','social_science': 'Social science','engineering': 'Petroleum engineering', 'business_marketing': 'Business and marketing', 'philosophy_religious': 'Philosophy', 'mathematics': 'Mathematics', 'language': 'Foreign languages', 'military': "None", 'family_consumer_science': 'Family and consumer science', 'engineering_technology': 'Systems engineering', 'science_technology': 'Medical laboratory technology', 'legal': 'Legal studies', 'humanities': 'Humanities', 'public_administration_social_service': 'Public administration', 'multidiscipline': 'None', 'agriculture': 'Agriculture', 'parks_recreation_fitness': 'Parks and recreation management', 'education': 'Education', 'history': 'History', 'personal_culinary': 'Culinary arts', 'health': 'Health', 'mechanic_repair_technology': 'None', 'biological': 'Biological sciences', 'communications_technology': 'Technical communication', 'security_law_enforcement': "None", 'construction': 'Building construction management', 'english': 'English Literature'}
    my_inverted_dict = dict(map(reversed, degree_map.items()))
    degree_names = degree_map.values()

    for data1 in degrees:
        if (data1['degree'] in degree_map.values()):
            data = data1['degree']
            results = []
            for c in colleges:
                if (c['top_program'] in degree_map.keys()):
                    if (degree_map[c['top_program']].lower() == data.lower()):
                        results.append((c['name'], c['city']))
            college_append = {}
            for x in range(1, min(1, len(results))):
                data1['college' + str(x)] = results[x][0]
                data1['city' + str(x)] = results[x][1]   
            print (data1)
            new = Degrees(**data1)
            db.session.add(new)
    db.session.commit()
    assert(False)


def upload_photo_url_data():
    # CREATING PHOTO URL DATA
    p = open("../photo_urls.p", "rb")
    urls = pickle.load(p)
    for key in urls:
        dic = {"state": key, "url": urls[key]}
        new = PhotoUrls(**dic)
        db.session.add(new)
    db.session.commit()


def upload_college_data():
    # CREATING COLLEGE DATA
    p = open("../college_data.p", "rb")
    college_data = pickle.load(p)
    for col in college_data:
        # DEMOGRAPHICS"""
        """
        keys = ['id', 'black', 'white', 'asian', 'hispanic', 'asian_pacific_islander']
        demo_info = {}
        for key in keys:
            demo_info[key] = col[key]
        demo = Demographics(**demo_info)
        demo_ref = db.session.add(demo)
        # COLLEGE
        keys = ['id', 'name', 'city', 'lon', 'lat', 'top_program', 'admission_rate', 'completion', 'sat_scores', 'in_state', 'out_of_state']
        degree_map = {'architecture': 'Architecture', 'transportation': 'Transportation and logistics management', 'visual_performing': 'Visual arts (VA)', "psychology": "Psychology", 'theology_religious_vocation': "Theology", 'computer': 'Computer science (CS) and engineering', 'physical_science': 'Physical therapy', 'communication': 'Communication','social_science': 'Social science','engineering': 'Petroleum engineering', 'business_marketing': 'Business and marketing', 'philosophy_religious': 'Philosophy', 'mathematics': 'Mathematics', 'language': 'Foreign languages', 'military': "None", 'family_consumer_science': 'Family and consumer science', 'engineering_technology': 'Systems engineering', 'science_technology': 'Medical laboratory technology', 'legal': 'Legal studies', 'humanities': 'Humanities', 'public_administration_social_service': 'Public administration', 'multidiscipline': 'None', 'agriculture': 'Agriculture', 'parks_recreation_fitness': 'Parks and recreation management', 'education': 'Education', 'history': 'History', 'personal_culinary': 'Culinary arts', 'health': 'Health', 'mechanic_repair_technology': 'None', 'biological': 'Biological sciences', 'communications_technology': 'Technical communication', 'security_law_enforcement': "None", 'construction': 'Building construction management', 'english': 'English Literature'}
        college_info = {}
        for key in keys:
            if (key == 'top_program'):
                if (col[key] in degree_map):
                    college_info[key] = degree_map[col[key]]
                else:
                    college_info[key] = "None"
            else:
                college_info[key] = col[key]

        #college_info["demographics"] = demo
        college = Colleges(**college_info)
        db.session.add(college)
    db.session.commit()
def upload_cities():
    # Code to insert city values into database
    p1 = open("../master_city_list.p", "rb")
    city_data = pickle.load(p1)
    for x in range(len(city_data)):
        city_data[x]['id'] = x
        c = Cities(**city_data[x])
        db.session.add(c)
    db.session.commit()
    assert(False)
"""

#Colleges.__table__.drop(engine)

#
#upload_cities()



class Colleges(db.Model):
    __tablename__ = "colleges"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(94))
    city = db.Column(db.String(34))
    lon = db.Column(db.Float)
    lat = db.Column(db.Float)
    top_program = db.Column(db.String(50))
    # Percentage Rates
    admission_rate = db.Column(db.Float)
    completion = db.Column(db.Float)
    sat_scores = db.Column(db.Float)
    # Prices
    in_state = db.Column(db.Integer)
    out_of_state = db.Column(db.Float)
    #demographics = db.relationship("Demographics", backref="colleges", lazy=True)

    #resources = db.Column(db.Float)
    #two_or_more = db.Column(db.Float)
    #degree_percentages = db.relationship("DegreesPercentages", backref="colleges", lazy=True)

class Demographics(db.Model):
    __tablename__ = "demo_ref"
    id = db.Column(db.Integer, primary_key=True)
    black = db.Column(db.Float)
    white = db.Column(db.Float)
    asian = db.Column(db.Float)
    hispanic = db.Column(db.Float)
    asian_pacific_islander = db.Column(db.Float)
    #ownership_peps = db.Column(db.Float)
    #nhpi = db.Column(db.Float)
    #aian = db.Column(db.Float)
    #non_resident_alien = db.Column(db.Float)
    #black_non_hispanic = db.Column(db.Float)
    #unknown = db.Column(db.Float)
    #white_non_hispanic = db.Column(db.Float)

"""
class DegreesPercentages(db.Model):
    __tablename__ = "degrees_ref"
    id = db.Column(db.Integer, primary_key=True)
    precision_production = db.Column(db.Float)
    architecture = db.Column(db.Float)
    transportation = db.Column(db.Float)
    ethnic_cultural_gender = db.Column(db.Float)
    visual_performing = db.Column(db.Float)
    library = db.Column(db.Float)
    psychology = db.Column(db.Float)
    theology_religious_vocation = db.Column(db.Float)
    computer = db.Column(db.Float)
    physical_science = db.Column(db.Float)
    communication = db.Column(db.Float)
    social_science = db.Column(db.Float)
    engineering = db.Column(db.Float)
    business_marketing = db.Column(db.Float)
    philosophy_religious = db.Column(db.Float)
    mathematics = db.Column(db.Float)
    language = db.Column(db.Float)
    military = db.Column(db.Float)
    family_consumer_science = db.Column(db.Float)
    engineering_technology = db.Column(db.Float)
    science_technology = db.Column(db.Float)
    legal = db.Column(db.Float)
    humanities = db.Column(db.Float)
    public_administration_social_service = db.Column(db.Float)
    multidiscipline = db.Column(db.Float)
    agriculture = db.Column(db.Float)
    parks_recreation_fitness = db.Column(db.Float)
    education = db.Column(db.Float)
    history = db.Column(db.Float)
    personal_culinary = db.Column(db.Float)
    health = db.Column(db.Float)
    mechanic_repair_technology = db.Column(db.Float)
    biological = db.Column(db.Float)
    communications_technology = db.Column(db.Float)
    security_law_enforcement = db.Column(db.Float)
    construction = db.Column(db.Float)
    english = db.Col