import sqlalchemy
import pandas as pd
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey
import pickle

def add_city(city_data):
    global connection
    global cities
    connection.execute(cities.insert(), city_data)

def show_table(results):
    df = pd.DataFrame(results)
    df.columns = results.keys()
    return df

url = 'mysql://{0}:{1}@{2}:{3}/degreeforme'.format('kate', 'sqlpassword', 'localhost', 5432)
engine = sqlalchemy.create_engine(url)
connection = engine.connect()
meta = MetaData()

cities = Table(
   'cities', meta, 
   Column('id', Integer, primary_key = True), 
   Column('name', String(23)), 
   Column('state', String(20)), 
   Column('overall', Integer),
   Column('grocery', Integer),
   Column('health', Integer),
   Column('housing', Integer),
   Column('median_home_cost', Integer),
   Column('utilities', Integer),
   Column('transportation', Integer),
   Column('misc', Integer)
)


"""
# Code to insert city values into database
p1 = open("master_city_list.p", "rb")
city_data = pickle.load(p1)

for x in range(len(city_data)):
  if (x < 1209):
    city_data[x]['id'] = x
    print (city_data[x]['id'])
    engine.execute(cities.insert(), city_data[x])
"""

engine.create_all()

p = open("degree_data.p", "rb")
degree_data = pickle.load(p)

print (meta.tables.keys())
colleges = meta.tables['colleges']

#engine.execute(colleges.insert(), college_data[0])

print (college_data[0].keys())
