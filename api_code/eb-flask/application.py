from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_restless import APIManager
from flask_cors import CORS, cross_origin

# EB looks for an 'application' callable by default.
application = Flask(__name__)
CORS(application)
application.config['CORS_HEADERS'] = 'Content-Type'


@application.route('/')
@cross_origin()
def help_page():
    header_text = '''<html>\n<head> <title>API Help</title> </head>\n<body>'''
    enpoints = '''
        <p><b><u>Enpoints</u></b><br/>
        /colleges<br/>
        /degrees<br/>
        /cities<br/>
        </p>\n'''
    link = "https://documenter.getpostman.com/view/7931031/S1a4Z84y?version=latest"
    footer_text = '''
    </body><p><b><u>Api Documentation</u></b><br/>
    <a href=\"''' + link + '''\"> Doc pages</a>
    \n</html>'''
    return header_text + enpoints + footer_text

def colleges_page():
    header_text = '''<html>\n<head> <title>API-Colleges</title> </head>\n<body>'''
    enpoints = '''
        <p><u>Work in progress</u><br/>
        </p>\n'''
    footer_text = '</body>\n</html>'
    return header_text + enpoints + footer_text

home_link = '<p><a href="/">Back</a></p>\n'

# Rules for varying webpages
#application.add_url_rule('/', 'index', help_page)

url = 'mysql://{0}:{1}@{2}:{3}/degreeforme'.format('admin', 'sqlpassword', 'dfm-database5.cluster-cxbtbcjlqmlo.us-east-1.rds.amazonaws.com', 5403)
#url = 'mysql://{0}:{1}@{2}:{3}/degreeforme'.format('kate', 'sqlpassword', 'localhost', 5432)
application.config['SQLALCHEMY_DATABASE_URI'] = url
db = SQLAlchemy(application)

class Cities(db.Model):
    __tablename__ = "cities"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(23))
    state = db.Column(db.String(20))
    overall = db.Column(db.Integer)
    grocery = db.Column(db.Integer)
    health = db.Column(db.Integer)
    housing = db.Column(db.Integer)
    median_home_cost = db.Column(db.Integer)
    utilities = db.Column(db.Integer)
    transportation = db.Column(db.Integer)
    misc = db.Column(db.Integer)
    photo_url = db.Column(db.Text)
    college = db.Column(db.String(75))
    degree = db.Column(db.String(39))

class Colleges(db.Model):
    __tablename__ = "colleges"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(94))
    city = db.Column(db.String(34))
    lon = db.Column(db.Float)
    lat = db.Column(db.Float)
    top_program = db.Column(db.String(50))
    photo_url = db.Column(db.String(305))
    # Percentage Rates
    admission_rate = db.Column(db.Float)
    completion = db.Column(db.Float)
    sat_scores = db.Column(db.Float)
    # Prices
    in_state = db.Column(db.Integer)
    out_of_state = db.Column(db.Float)

class Demographics(db.Model):
    __tablename__ = "demo_ref"
    id = db.Column(db.Integer, primary_key=True)
    black = db.Column(db.Float)
    white = db.Column(db.Float)
    asian = db.Column(db.Float)
    hispanic = db.Column(db.Float)
    asian_pacific_islander = db.Column(db.Float)

class PhotoUrls(db.Model):
    __tablename__ = "state_photo_urls"
    state = db.Column(db.String(20), primary_key = True)
    url = db.Column(db.String(59))

class Degrees(db.Model):
    __tablename__ = "degrees"
    degree = db.Column(db.String(40), primary_key = True)
    rank = db.Column(db.Integer)
    meaningful_work = db.Column(db.Float)
    pay_early = db.Column(db.Integer)
    pay_mid = db.Column(db.Integer)
    college1 = db.Column(db.String(94))
    city1 = db.Column(db.String(40))
    photo_url = db.Column(db.String(140))

#db.create_all()
stuff = APIManager(application, flask_sqlalchemy_db=db)
cities_api = stuff.create_api(Cities, methods=['GET'], results_per_page=9)
colleges_api = stuff.create_api(Colleges, methods=['GET'], results_per_page=9)
photo_url_api = stuff.create_api(PhotoUrls, methods=['GET'], results_per_page=9)
degrees_api = stuff.create_api(Degrees, methods=['GET'], results_per_page=9)

# run the app.
if __name__ == "__main__":
    # Setting debug to True enables debug output. This line should be
    # removed before deploying a production app.
    application.debug = True
    application.run(host = '0.0.0.0')