{
  "num_results": 50, 
  "objects": [
    {
      "state": "alabama", 
      "url": "https://img.bestplaces.net/images/states/alabama.jpg"
    }, 
    {
      "state": "alaska", 
      "url": "https://img.bestplaces.net/images/states/alaska.jpg"
    }, 
    {
      "state": "arizona", 
      "url": "https://img.bestplaces.net/images/states/arizona.jpg"
    }, 
    {
      "state": "arkansas", 
      "url": "https://img.bestplaces.net/images/states/arkansas.jpg"
    }, 
    {
      "state": "california", 
      "url": "https://img.bestplaces.net/images/states/california.jpg"
    }, 
    {
      "state": "colorado", 
      "url": "https://img.bestplaces.net/images/states/colorado.jpg"
    }, 
    {
      "state": "connecticut", 
      "url": "https://img.bestplaces.net/images/states/connecticut.jpg"
    }, 
    {
      "state": "delaware", 
      "url": "https://img.bestplaces.net/images/states/delaware.jpg"
    }, 
    {
      "state": "florida", 
      "url": "https://img.bestplaces.net/images/states/florida.jpg"
    }, 
    {
      "state": "georgia", 
      "url": "https://img.bestplaces.net/images/states/georgia.jpg"
    }
  ], 
  "page": 1, 
  "total_pages": 5
}
