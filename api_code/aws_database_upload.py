from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import pickle
import sqlalchemy


app = Flask(__name__)
url = 'mysql://{0}:{1}@{2}:{3}/degreeforme'.format('admin', 'sqlpassword', 'dfm-database5.cluster-cxbtbcjlqmlo.us-east-1.rds.amazonaws.com', 5403)
#url = 'mysql://{0}:{1}@{2}:{3}/degreeforme'.format('kate', 'sqlpassword', 'localhost', 5432)
app.config['SQLALCHEMY_DATABASE_URI'] = url
db = SQLAlchemy(app)
engine = sqlalchemy.create_engine(url)


class Cities(db.Model):
    __tablename__ = "cities"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(23))
    state = db.Column(db.String(20))
    overall = db.Column(db.Integer)
    grocery = db.Column(db.Integer)
    health = db.Column(db.Integer)
    housing = db.Column(db.Integer)
    median_home_cost = db.Column(db.Integer)
    utilities = db.Column(db.Integer)
    transportation = db.Column(db.Integer)
    misc = db.Column(db.Integer)
    photo_url = db.Column(db.Text)
    college = db.Column(db.String(75))
    degree = db.Column(db.String(39))


class Colleges(db.Model):
    __tablename__ = "colleges"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(94))
    city = db.Column(db.String(34))
    lon = db.Column(db.Float)
    lat = db.Column(db.Float)
    top_program = db.Column(db.String(50))
    photo_url = db.Column(db.String(305))
    # Percentage Rates
    admission_rate = db.Column(db.Float)
    completion = db.Column(db.Float)
    sat_scores = db.Column(db.Float)
    # Prices
    in_state = db.Column(db.Integer)
    out_of_state = db.Column(db.Float)

class Demographics(db.Model):
    __tablename__ = "demo_ref"
    id = db.Column(db.Integer, primary_key=True)
    black = db.Column(db.Float)
    white = db.Column(db.Float)
    asian = db.Column(db.Float)
    hispanic = db.Column(db.Float)
    asian_pacific_islander = db.Column(db.Float)

class PhotoUrls(db.Model):
    __tablename__ = "state_photo_urls"
    state = db.Column(db.String(20), primary_key = True)
    url = db.Column(db.String(59))

class Degrees(db.Model):
    __tablename__ = "degrees"
    degree = db.Column(db.String(40), primary_key = True)
    rank = db.Column(db.Integer)
    meaningful_work = db.Column(db.Float)
    pay_early = db.Column(db.Integer)
    pay_mid = db.Column(db.Integer)
    college1 = db.Column(db.String(94))
    city1 = db.Column(db.String(40))
    photo_url = db.Column(db.String(140))


# UPLOADING CODE

def upload_degree_data():
    p = open("refined_degree_data2.p", "rb")
    degrees = pickle.load(p)
    for data1 in degrees:
        new = Degrees(**data1)
        db.session.add(new)
    db.session.commit()


def upload_photo_url_data():
    # CREATING PHOTO URL DATA
    p = open("../photo_urls.p", "rb")
    urls = pickle.load(p)
    for key in urls:
        dic = {"state": key, "url": urls[key]}
        new = PhotoUrls(**dic)
        db.session.add(new)
    db.session.commit()


def upload_college_data():
    # CREATING COLLEGE DATA
    p = open("refined_college_data3.p", "rb")
    college_data = pickle.load(p)
    for col in college_data:
        # DEMOGRAPHICS
        """
        keys = ['id', 'black', 'white', 'asian', 'hispanic', 'asian_pacific_islander']
        demo_info = {}
        for key in keys:
            demo_info[key] = col[key]
        demo = Demographics(**demo_info)
        demo_ref = db.session.add(demo)
        """
        # COLLEGE
        keys = ['id', 'name', 'city', 'lon', 'lat', 'top_program', 'admission_rate', 'completion', 'sat_scores', 'in_state', 'out_of_state', 'photo_url']
        college_info = {}
        for key in keys:
            college_info[key] = col[key]
        college = Colleges(**college_info)
        db.session.add(college)
    db.session.commit()

def upload_cities():
    # Code to insert city values into database
    p1 = open("refined_city_data3.p", "rb")
    city_data = pickle.load(p1)
    for x in range(len(city_data)):
        new = city_data[x]
        new['id'] = x + 1
        print (new['id'])
        c = Cities(**city_data[x])
        db.session.add(c)
    db.session.commit()
    assert(False)

# Drops a given table
#db.create_all()
#Cities.__table__.drop(engine)
Degrees.__table__.drop(engine)
#db.session.commit()
db.create_all()
upload_degree_data()

def get_colleges_for_cities():
    college_data = pickle.load(open("refined_college_data.p", "rb"))
    result = []
    for c in college_data:
        if (c['admission_rate'] != "None" and c['completion'] != "None" and c['sat_scores'] != "None"):
            result.append(c)
    pickle.dump(result, open("refined_college_data2.p", "wb"))
    print (result)

